#pragma once

#include "Utils/Hash.h"
#include "Utils/Logging.h"
#include "PropertyHolder.h"
#include "PTypes.h"


class PropertyHolder;

template<typename T>
class Property;

template<typename T>
class PropertyRef
{
private:
	Property<T> * p;
	PropertyHolder & ph;

public:
	PropertyRef(
		PropertyHolder & properties,
		const T & value,
		const string & name,
		const string & desc = "",
		bool readOnly = false);

	~PropertyRef()
	{
	}

	T & Get() const { return *p->template Get<T>(); }
	string GetName() const { return p->GetName(); }
	string GetDesc() const { return p->GetDesc(); }
	bool GetIsReadOnly() const { return p->GetIsReadOnly(); }

	operator T() const { return *p->template Get<T>(); }

	PropertyRef & operator=(const T & other)
	{
		p->Set(other);
		return *this;
	}

	PropertyRef & operator=(const PropertyRef<T> & other)
	{
		p->Set(other);
		return *this;
	}

	template<typename Other>
	bool operator==(const Other & other) { return *p->template Get<T>() == other; }

	T * operator->() { return p->template Get<T>(); }

	auto & operator[](unsigned i) { return (*p->template Get<T>())[i]; }

	T operator++() { return ++*(p->template Get<T>()); }

	T operator++(int)
	{
		T t = *(p->template Get<T>());
		p->Set(t + 1); // not exactly t++
		return t;
	}

	T operator--() { return --*(p->template Get<T>()); }

	T operator--(int)
	{
		T t = *(p->template Get<T>());
		p->Set(t - 1); // not exactly t--
		return t;
	}

	template<typename Other>
	auto operator+(const PRef<Other> & other) const { return Get() + other.Get(); }

	template<typename Other>
	auto operator+(const Other & other) const { return Get() + other; }

	template<typename Other>
	auto operator*(const PRef<Other> & other) const { return Get() * other.Get(); }

	template<typename Other>
	auto operator*(const Other & other) const { return Get() * other; }
};


#include "Property.h"


template<typename T>
inline PropertyRef<T>::PropertyRef(PropertyHolder & properties, const T & value, const string & name, const string & desc, bool readOnly) :
	p(nullptr),
	ph(properties)
{
	Property<T> * prop = (Property<T> *)properties.Get(name);
	if (!prop)
	{
		p = (Property<T> *)ph.Add((PropertyBase *)new Property<T>(value, name, desc, readOnly));
		return;
	}
	ASSERT(*prop->GetType() == *PType<T>::GetInstance());
	//*prop = Property<T>(value, name, desc, readOnly);
	p = prop;
}
