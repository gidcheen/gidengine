#pragma once

#include "PropertyBase.h"


template<typename T>
class Property : public PropertyBase
{
public:
	Property(const T & value, const string & name, const string & desc = "", bool readOnly = false);

	Property(const Json::Value & value) :
		PropertyBase(value),
		dataRef((T *)data)
	{
	}

private:
	T * dataRef;
};


#include "PType.h"

template<typename T>
inline Property<T>::Property(const T & value, const string & name, const string & desc, bool readOnly) :
	PropertyBase(PType<T>::GetInstance(), new T(value), name, desc, readOnly),
	dataRef((T *)data)
{
}
