#pragma once

#include <string>
#include <vector>
#include <json/json.h>


using namespace std;

class PropertyBase;
class TypeBase;

class PropertyHolder
{
public:
	PropertyBase * Add(PropertyBase * property);
	bool Remove(PropertyBase * property);

	PropertyBase * Get(const string & name) const;

	inline const vector<PropertyBase *> & GetAll() const
	{
		return properties;
	}

	const vector<PropertyBase *> GetAllOfType(const TypeBase * type) const;

	Json::Value ToJson() const;
	void SetFromJson(const Json::Value & json);

private:
	vector<PropertyBase *> properties;
};
