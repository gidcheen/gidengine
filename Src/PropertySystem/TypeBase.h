#pragma once

#include <string>
#include <utility>
#include <vector>
#include <functional>
#include <json/json.h>

#include "Utils/Hash.h"
#include "Utils/Logging.h"
#include "Utils/Assert.h"


using namespace std;

using ToJsonF = function<Json::Value(const void *)>;
using FromJsonF = function<void *(const Json::Value &)>;

class TypeBase
{
public:
	const ID id;
	const string name;
	const ToJsonF toJson;
	const FromJsonF fromJson;

private:
	static vector<TypeBase *> * types;

public:
	TypeBase(const string & name, const ToJsonF & toJson, const FromJsonF & fromJson) :
		name(name),
		id(ToHash(name)),
		toJson(toJson),
		fromJson(fromJson)
	{
		if (!types)
		{
			static vector<TypeBase *> typesS;
			types = &typesS;
		}
		types->push_back(this);
	};

	virtual ~TypeBase() {};

	bool operator==(const TypeBase & other) const { return id == other.id; }
	bool operator==(ID other) const { return this->id == other; }
	bool operator!=(const TypeBase & other) const { return id != other.id; }
	bool operator!=(ID other) const { return this->id != other; }

	inline static const TypeBase * GetType(const string & name) { return GetType(ToHash(name)); }
	inline static const TypeBase * GetType(ID id)
	{
		for (TypeBase * tb : *types)
		{
			if (tb->id == id)
			{
				return tb;
			}
		}
		ASSERT(false);
		return nullptr;
	}

	virtual void * Create(void * value) const = 0;
	virtual void Delete(void * value) const = 0;
};
