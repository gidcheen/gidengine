#pragma once

#include "TypeBase.h"
#include <json/json.h>

template<typename>
class PType;

#define STR(s) #s
#define CONCATENATION(a,b) a##b




#define TO_JSON_VECTOR_FUNC(typeT, toJsonT)												\
[](const void * data) -> Json::Value													\
{																						\
	vector<typeT> * d = (vector<typeT> *)data;											\
	Json::Value ret;																	\
	for (const typeT & e : *d)															\
	{																					\
		ret.append(toJsonT(&e));														\
	}																					\
	return ret;																			\
}																						




#define FROM_JSON_VECTOR_FUNC(typeT, fromJsonT)											\
[](const Json::Value & data) -> void *													\
{																						\
	vector<typeT> * ret = new vector<typeT>();											\
	for (const Json::Value & e : data)													\
	{																					\
		typeT * v = (typeT *)fromJsonT(e);												\
		ret->push_back(*v);																\
		delete v;																		\
	}																					\
	return (void *)ret;																	\
}																						\




#define TYPE(typeT, toJsonT, fromJsonT)	 												\
template<>																				\
class PType<typeT> : public TypeBase													\
{																						\
private:																				\
	static PType<typeT> * instance;														\
																						\
public:																					\
	PType() :																			\
		TypeBase(STR(typeT), toJsonT, fromJsonT)										\
	{																					\
	}																					\
																						\
	static const PType<typeT> * GetInstance()											\
	{																					\
		if (!instance)																	\
		{																				\
			instance = (PType<typeT> *)GetType(STR(typeT));								\
		}																				\
		return instance;																\
	}																					\
																						\
	virtual void * Create(void * value) const override									\
	{																					\
		return new typeT(*(typeT *)value);												\
	}																					\
																						\
	virtual void Delete(void * value) const override									\
	{																					\
		delete (typeT *)value;															\
	}																					\
};																						\
typedef PRef<typeT> CONCATENATION(P, typeT);											\
extern PType<typeT> CONCATENATION(typeT, Type);											\




#define PTYPE(typeT, toJsonT, fromJsonT)												\
	TYPE(typeT, toJsonT, fromJsonT)														\
	typedef vector<typeT> CONCATENATION(typeT, Vector);									\
	TYPE(																				\
		CONCATENATION(typeT, Vector),													\
		TO_JSON_VECTOR_FUNC(int, toJsonT),												\
		FROM_JSON_VECTOR_FUNC(typeT, fromJsonT)											\
	)																					\

