#pragma once

#include "PType.h"
#include "Maths/Maths.h"
#include "Assets/AssetManagement/AssetRef.h"
#include "Assets/Texture.h"


template<typename T>
class PropertyRef;

template<typename T> using PRef = PropertyRef<T>;


// todo: see if instance can can simply be given directly here
#define PTYPE_INSTANCE(typeT) \
PType<typeT> typeT##Type; \
PType<typeT> * PType<typeT>::instance(nullptr); \
PType<typeT##Vector> typeT##Vector##Type; \
PType<typeT##Vector> * PType<typeT##Vector>::instance(nullptr); \


typedef int Int;
PTYPE(Int,
	  [](const void * data)->Json::Value { return Json::Value(*(int *)data); },
	  [](const Json::Value & data)->void * { return (void *)new Int(data.asInt()); });


typedef float Float;
PTYPE(Float,
	  [](const void * data)->Json::Value { return Json::Value(*(Float *)data); },
	  [](const Json::Value & data)->void * { return new Float(data.asFloat()); });


typedef double Double;
PTYPE(Double,
	  [](const void * data)->Json::Value { return Json::Value(*(Double *)data); },
	  [](const Json::Value & data)->void * { return new Double(data.asDouble()); });


typedef bool Bool;
PTYPE(Bool,
	  [](const void * data)->Json::Value { return Json::Value(*(Bool *)data); },
	  [](const Json::Value & data)->void * { return new Int(data.asBool()); });


PTYPE(ID,
	  [](const void * data)->Json::Value { return Json::Value(*(Json::UInt64 *)data); },
	  [](const Json::Value & data)->void *
	  {
		  if (data.isString())
		  {
			  return new ID(ToHash(data.asString()));
		  }
		  else
		  {
		  	return new ID(data.asUInt64());
		  }
	  });

typedef char Char;
PTYPE(Char,
	  [](const void * data)->Json::Value { return Json::Value(*(Char *)data);},
	  [](const Json::Value & data)->void * { return new Char(data.asInt()); });


typedef string String;
PTYPE(String,
	  [](const void * data)->Json::Value { return Json::Value(*(String *)data);},
	  [](const Json::Value & data)->void * { return new String(data.asString()); });



PTYPE(Vec2,
	  [](const void * data)->Json::Value
	  {
		  Vec2 * v = (Vec2 *)data;
		  Json::Value json;
		  json["x"] = v->x;
		  json["y"] = v->y;
		  return json;
	  },
	  [](const Json::Value & data)->void * { return new Vec2(data["x"].asFloat(), data["y"].asFloat()); });


PTYPE(Vec3,
	  [](const void * data)->Json::Value
	  {
		  Vec3 * v = (Vec3 *)data;
		  Json::Value json;
		  json["x"] = v->x;
		  json["y"] = v->y;
		  json["z"] = v->z;
		  return json;
	  },
	  [](const Json::Value & data)->void *
	  {
		  return new Vec3(data["x"].asFloat(), data["y"].asFloat(), data["z"].asFloat());
	  });


PTYPE(Vec4,
	  [](const void * data)->Json::Value
	  {
		  Vec4 * v = (Vec4 *)data;
		  Json::Value json;
		  json["x"] = v->x;
		  json["y"] = v->y;
		  json["z"] = v->z;
		  json["w"] = v->w;
		  return json;
	  },
	  [](const Json::Value & data)->void *
	  {
		  return new Vec4(data["x"].asFloat(), data["y"].asFloat(), data["z"].asFloat(), data["w"].asFloat());
	  });


PTYPE(Mat4,
	  [](const void * data)->Json::Value
	  {
		  Mat4 * v = (Mat4 *)data;
		  Json::Value json;
		  for (int i = 0; i < 16; i++)
		  {
			  json.append((*v)[i]);
		  }
		  return json;
	  },
	  [](const Json::Value & data)->void *
	  {
		  Mat4 * m = new Mat4();
		  for (int i = 0; i < 16; i++)
		  {
			  (*m).elements[i] = data[i].asFloat();
		  }
		  return m;
	  });

typedef AssetRef<Texture> TextureRef;
PTYPE(TextureRef,
	  [](const void * data)->Json::Value
	  {
		  TextureRef * t = (TextureRef *)data;
		  return Json::Value(*(Json::UInt64 *)t->GetId());
	  },
	  [](const Json::Value & data)->void *
	  {
		  if (data.isString())
		  {
			  return new TextureRef(data.asString());
		  }
		  else if (data.isUInt64() || data.isNull())
		  {
			  return new TextureRef(data.asUInt64());
		  }
	  });


