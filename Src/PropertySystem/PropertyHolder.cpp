#include "PropertyHolder.h"

#include "Property.h"
#include "TypeBase.h"


PropertyBase * PropertyHolder::Add(PropertyBase * property)
{
	ASSERT(Get(property->GetName()) == nullptr);
	properties.push_back(property);
	return property;
}

bool PropertyHolder::Remove(PropertyBase * property)
{
	int propertiesSize = (int)properties.size();
	for (int i = 0; i < propertiesSize; i++)
	{
		PropertyBase * p = properties[i];
		if (p == property)
		{
			delete p;
			properties.erase(properties.begin() + i);
			return true;
		}
	}
	return false;
}

PropertyBase * PropertyHolder::Get(const string & name) const
{
	for (PropertyBase * p : properties)
	{
		if (p->GetName() == name)
		{
			return p;
		}
	}
	return nullptr;
}

const vector<PropertyBase *> PropertyHolder::GetAllOfType(const TypeBase * type) const
{
	vector<PropertyBase *> ret;
	for (PropertyBase * p : properties)
	{
		if (*p->GetType() == *type)
		{
			ret.push_back(p);
		}
	}
	return ret;
}

Json::Value PropertyHolder::ToJson() const
{
	Json::Value json;
	for (const auto * p : properties)
	{
		json.append(p->ToJson());
	}
	return json;
}

void PropertyHolder::SetFromJson(const Json::Value & json)
{
	for (const auto & propertyJson : json)
	{
		PropertyBase * p = Get(propertyJson["name"].asString());
		if (p)
		{
			*p = PropertyBase(propertyJson);
		}
		else
		{
			Add(new PropertyBase(propertyJson));
		}
	}
}
