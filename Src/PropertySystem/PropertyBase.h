#pragma once

#include <string>
#include <vector>
#include <json/json.h>

#include "Maths/Maths.h"
#include "Utils/Assert.h"
#include "Utils/Hash.h"
#include "TypeBase.h"

using namespace std;


class PropertyBase
{
public:
	PropertyBase(const TypeBase * pType, void * value, const string & name, const string & desc = "", bool readOnly = false) :
		type(pType),
		data(value),
		name(name),
		desc(desc),
		readOnly(readOnly)
	{
	}

	PropertyBase(const Json::Value & value) :
		type(TypeBase::GetType(value["type"].asString())),
		data(type->fromJson(value["value"])),
		name(value["name"].asString()),
		desc(value.get("desc", "").asString()),
		readOnly(value.get("readOnly", false).asBool())
	{
	}

	PropertyBase(const PropertyBase & other) :
		type(other.type),
		data(type->Create(other.data)),
		name(other.name),
		desc(other.desc),
		readOnly(other.readOnly)
	{
	}

	virtual ~PropertyBase()
	{
		type->Delete(data);
	};

	PropertyBase & operator=(const PropertyBase & other)
	{
		ASSERT(*type == *other.type);
		ASSERT(name == other.name);
		ASSERT(!readOnly);

		type->Delete(data);
		data = type->Create(other.data);

		return *this;
	}

	Json::Value ToJson() const
	{
		Json::Value json;
		json["type"] = type->name;
		json["value"] = type->toJson(data);
		json["name"] = name;
		json["desc"] = desc;
		json["redOnly"] = readOnly;
		return json;
	}

	string ToString()
	{
		Json::FastWriter writer;
		return writer.write(ToJson());
	}

	template<typename T>
	inline T * Get() const
	{
		return (T *)data;
	}

	template<typename T>
	inline void Set(const T & value)
	{
		T * dataRef((T *)data);
		*dataRef = value;
	}

	inline const TypeBase * GetType()
	{
		return type;
	}

	inline string GetName() const
	{
		return name;
	}

	inline string GetDesc() const
	{
		return desc;
	}

	inline bool GetIsReadOnly() const
	{
		return readOnly;
	}

protected:
	const TypeBase * type;
	void * data;

private:
	string name;
	string desc;
	bool readOnly;
};
