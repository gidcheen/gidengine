#pragma once

#include "Maths/Maths.h"
#include "Graphics/Buffers/GLVertexArray.h"
#include "Graphics/Vertex.h"
#include "AssetManagement/AssetRef.h"
#include "Assets/AssetBase.h"
#include "Material.h"


struct MeshData
{
	Vertex * vertices;
	GLuint vertexCount;
	vector<GLuint *> indices;
	vector<GLuint> indexCount;

	MeshData(Vertex * vertices,
			 GLuint vertexCount,
			 const vector<GLuint *> & indices,
			 const vector<GLuint> & indexCount) :
		vertices(vertices),
		vertexCount(vertexCount),
		indices(indices),
		indexCount(indexCount) {}

	~MeshData()
	{
		delete vertices;
		for (auto i : indices) { delete i; }
	}
};

class Mesh : public AssetBase
{
NAME(Mesh)
public:
	GLVertexArray vertexArray;
	vector<GLIndexBuffer *> indexBuffers;

	Mesh(const MeshData & meshData);
	virtual ~Mesh();

	void BindBuffers(int materialIndex) const;
	void UnbindBuffers(int materialIndex) const;

	inline int GetMaterialCount() const { return (int)indexBuffers.size(); }
};
