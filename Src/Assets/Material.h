#pragma once

#include <json/json.h>

#include "Assets/AssetManagement/AssetRef.h"
#include "Assets/AssetBase.h"
#include "PropertySystem/PropertySystem.h"
#include "Utils/Hash.h"
#include "Assets/Shader.h"
#include "Assets/Texture.h"


class Material : public AssetBase
{
NAME(Material)
public:
	enum Transparency
	{
		opaque = 0, cutout, translucent
	};

private:
	AssetRef<Shader> shader;
	Transparency transparency;

	PropertyHolder properties;

	vector<PropertyBase *> ints;
	vector<PropertyBase *> bools;
	vector<PropertyBase *> floats;
	vector<PropertyBase *> vec2s;
	vector<PropertyBase *> vec3s;
	vector<PropertyBase *> vec4s;
	vector<PropertyBase *> textures;

public:
	Material(const AssetRef<Shader> & shaderAsset, Transparency transparency = Transparency::opaque);
	~Material() {};

	inline Transparency GetTransparency() const { return transparency; }
	inline void SetTransparency(Transparency t) { transparency = t; }

	void SetInt(int value, const string & name);
	void SetBool(bool value, const string & name);
	void SetFloat(float value, const string & name);
	void SetVec4(const Vec4 & value, const string & name);
	void SetTexture(ID value, const string & name);
	void SetTexture(const string & value, const string & name) { SetTexture(ToHash(value), name); };

	void SetProperty(PropertyBase * p);

	void SetShaderProperties() const;

	inline const Shader * GetShader() const { return shader.GetAsset(); };
};
