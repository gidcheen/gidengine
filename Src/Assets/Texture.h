#pragma once

#include "Assets/AssetBase.h"

#include "Graphics/GLTexture.h"
#include "Utils/Logging.h"


class Texture : public AssetBase
{
NAME(Texture)
private:
	GLTexture glTexture;

public:
	Texture(
		unsigned char * data,
		int width,
		int height,
		int channelCount,
		bool isCubeMap = false,
		Wrapping wrapping = Wrapping::repeat,
		Filtering filtering = Filtering::linear) :
		glTexture(data, width, height, GLTexture::TextureSettings::Create(channelCount, isCubeMap, wrapping, filtering)) {}

	inline const GLTexture * GetGlTexture() const { return &glTexture; }
	inline unsigned int GetTextureId() const { return glTexture.GetTextureId(); };

	inline int GetWidth() const { return glTexture.GetWidth(); }
	inline int GetHeight() const { return glTexture.GetHeight(); }

	inline void Bind() const { glTexture.Bind(); }
	inline void Unbind() const { glTexture.Unbind(); }

private:
};
