#include "Collision.h"

#include "GameHeader.h"


Collision::Collision(vector<PropertyBase *> ps)
{
	for (PropertyBase * p : ps)
	{
		properties.Add(p);
	}

	collisionType = *properties.Get("type")->Get<ID>();

	PropertyBase * pos = properties.Get("position");
	PropertyBase * rot = properties.Get("rotation");
	Vec3 position = pos ? *pos->Get<Vec3>() : Vec3();
	Vec3 rotation = rot ? *rot->Get<Vec3>() : Vec3();
	defaultTransform = Mat4::Transform(position, rotation, Vec3(1));

	PropertyBase * weight = properties.Get("weight");
	defaultWeight = weight ? *weight->Get<int>() : 1;

	if (collisionType == ToHash("Box"))
	{
		float x = *properties.Get("x")->Get<float>();
		float y = *properties.Get("y")->Get<float>();
		float z = *properties.Get("z")->Get<float>();
		collisionShape = new rp::BoxShape(rp::Vector3(x, y, z));
	}
	else if (collisionType == ToHash("Sphere"))
	{
		float radius = *properties.Get("radius")->Get<float>();
		collisionShape = new rp::SphereShape(radius);
	}
}

PTYPE_INSTANCE(CollisionRef);
