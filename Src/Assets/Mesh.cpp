#include "Mesh.h"


Mesh::Mesh(const MeshData & meshData)
{
	GLVertexBuffer * vbo = new GLVertexBuffer(&meshData.vertices[0].position.elements[0],
										  meshData.vertexCount * (sizeof(Vertex) / sizeof(float)),
										  meshData.vertexCount);
	vertexArray.AddVertexBuffer(vbo, 0, 3, 17, 0);
	vertexArray.AddVertexBuffer(vbo, 1, 3, 17, 3);
	vertexArray.AddVertexBuffer(vbo, 2, 3, 17, 6);
	vertexArray.AddVertexBuffer(vbo, 3, 3, 17, 9);
	vertexArray.AddVertexBuffer(vbo, 4, 3, 17, 12);
	vertexArray.AddVertexBuffer(vbo, 5, 2, 17, 15);

	for (int i = 0; i < meshData.indices.size(); i++)
	{
		indexBuffers.push_back(new GLIndexBuffer(meshData.indices[i], meshData.indexCount[i]));
	}
}

Mesh::~Mesh()
{
	for (GLIndexBuffer * ibo : indexBuffers)
	{
		delete ibo;
	}
}

void Mesh::BindBuffers(int materialIndex) const
{
	vertexArray.Bind();
	indexBuffers[materialIndex]->Bind();
}

void Mesh::UnbindBuffers(int materialIndex) const
{
	indexBuffers[materialIndex]->Unbind();
	vertexArray.Unbind();
}
