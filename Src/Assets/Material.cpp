#include "Material.h"


Material::Material(const AssetRef<Shader> & shaderAsset, Transparency transparency) :
	shader(shaderAsset),
	transparency(transparency)
{
	for (const auto & p : shader->GetDefaultProperties())
	{
		PropertyBase * newP = new PropertyBase(*p);
		const TypeBase * type = newP->GetType();
		properties.Add(newP);
		if (*type == IntType)
		{
			ints.push_back(newP);
		}
		else if(*type == BoolType)
		{
			bools.push_back(newP);
		}
		else if(*type == FloatType)
		{
			floats.push_back(newP);
		}
		else if(*type == Vec2Type)
		{
			vec2s.push_back(newP);
		}
		else if(*type == Vec3Type)
		{
			vec3s.push_back(newP);
		}
		else if(*type == Vec4Type)
		{
			vec4s.push_back(newP);
		}
		else if(*type == TextureRefType)
		{
			textures.push_back(newP);
		}
	}
}

void Material::SetInt(int value, const string & name)
{
	SetProperty(new Property<int>(value, name));
}

void Material::SetBool(bool value, const string & name)
{
	SetProperty(new Property<bool>(value, name));
}

void Material::SetFloat(float value, const string & name)
{
	SetProperty(new Property<float>(value, name));
}

void Material::SetVec4(const Vec4 & value, const string & name)
{
	SetProperty(new Property<Vec4>(value, name));
}

void Material::SetTexture(ID value, const string & name)
{
	SetProperty(new Property<TextureRef>(value, name));
}

void Material::SetProperty(PropertyBase * p)
{
	PropertyBase * existingP = properties.Get(p->GetName());
	if (!existingP)
	{
		LOG("Trying to set non existing proerty on material");
		delete p;
		return;
	}
	*existingP = *p;
	delete p;
}

void Material::SetShaderProperties() const
{
	string m("material.");
	shader->SetUniformI(m + "transparency", transparency);

	for (auto & p : ints)
	{
		shader->SetUniformI(m + p->GetName(), *p->Get<int>());
	}
	for (auto & p : bools)
	{
		shader->SetUniformI(m + p->GetName(), *p->Get<bool>());
	}
	for (auto & p : floats)
	{
		shader->SetUniformF(m + p->GetName(), *p->Get<float>());
	}
	for (auto & p : vec2s)
	{
		shader->SetUniformV2(m + p->GetName(), *p->Get<Vec2>());
	}
	for (auto & p : vec3s)
	{
		shader->SetUniformV3(m + p->GetName(), *p->Get<Vec3>());
	}
	for (auto & p : vec4s)
	{
		shader->SetUniformV4(m + p->GetName(), *p->Get<Vec4>());
	}
	int i = 0;
	for (auto & p : textures)
	{
		TextureRef * tex = p->Get<TextureRef>();
		if (tex->IsEmpty()) { continue; }
		shader->SetTexture(m + p->GetName(), tex->GetAsset()->GetGlTexture(), i);
		i++;
	}
}
