#pragma once

#include <iostream>
#include <GL/glew.h>
#include <vector>
#include <string>

#include "Assets/AssetBase.h"
#include "Maths/Maths.h"
#include "PropertySystem/PropertyHolder.h"
#include "Graphics/GLShader.h"


class GLTexture;

using namespace std;

class Shader : public AssetBase
{
NAME(Shader)

private:
	PropertyHolder properties;
	GLShader glShader;

public:
	Shader(const GLShader::ShaderSrcs & srcs, const vector<PropertyBase *> & defaults);
	~Shader() = default;

	const vector<PropertyBase *> & GetDefaultProperties() const { return properties.GetAll(); }

	void SetUniformF(const string & name, float value) const { glShader.SetUniformF(name, value); }
	void SetUniformI(const string & name, int value) const { glShader.SetUniformI(name, value); }
	void SetUniformV2(const string & name, const Vec2 & value) const { glShader.SetUniformV2(name, value); }
	void SetUniformV3(const string & name, const Vec3 & value) const { glShader.SetUniformV3(name, value); }
	void SetUniformV4(const string & name, const Vec4 & value) const { glShader.SetUniformV4(name, value); }
	void SetUniformMat4(const string & name, const Mat4 & value) const { glShader.SetUniformMat4(name, value); }
	void SetTexture(const string & name, const GLTexture * texture, int position) const { glShader.SetTexture(name, texture, position); };

	void Enable() const { glShader.Enable(); }
	void Disable() const { glShader.Disable(); }
};
