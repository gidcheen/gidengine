#pragma once

#include "Assets/AssetManagement/AssetPipeline.h"
#include "Utils/Hash.h"
#include "Utils/Named.h"

class AssetBase : public Named
{
	friend class AssetManager;

private:
	ID id; // todo: getter
	bool isRefCounted;
	int refCnt;

public:
	AssetBase() :
		id(0),
		isRefCounted(false),
		refCnt(0) {}
	virtual ~AssetBase() {}

	inline ID GetId() const { return id; }
};
