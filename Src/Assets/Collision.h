#pragma once

#include "AssetBase.h"

#include "PropertySystem/PropertySystem.h"

namespace reactphysics3d
{
	class CollisionShape;
}
namespace rp = reactphysics3d;


class Collision : public AssetBase
{
NAME(Collision)
public:

private:
	rp::CollisionShape * collisionShape;
	ID collisionType;
	Mat4 defaultTransform;
	int defaultWeight;

	PropertyHolder properties;

public:
	Collision(vector<PropertyBase *> ps);

	rp::CollisionShape * GetShape() { return collisionShape; }
	ID GetCollisionType() { return collisionType; }
	const Mat4 & GetDefaultTransform() { return defaultTransform; }
	int GetDefaultWeight() { return defaultWeight; }
};

typedef AssetRef<Collision> CollisionRef;
PTYPE(CollisionRef,
	  [](const void * data)->Json::Value
	  {
		  TextureRef * t = (TextureRef *)data;
		  return Json::Value(*(Json::UInt64 *)t->GetId());
	  },
	  [](const Json::Value & data)->void *
	  {
		  if (data.isString())
		  {
			  return new TextureRef(data.asString());
		  }
		  else if (data.isUInt64())
		  {
			  return new TextureRef(data.asUInt64());
		  }
	  });