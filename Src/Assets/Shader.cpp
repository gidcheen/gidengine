#include "Shader.h"

#include "Utils/StringUtils.h"


Shader::Shader(const GLShader::ShaderSrcs & srcs, const vector<PropertyBase *> & defaults) :
	glShader(srcs)
{
	for (auto * s : defaults)
	{
		properties.Add(s);
	}
}
