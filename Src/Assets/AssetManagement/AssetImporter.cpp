#include "AssetImporter.h"

#include <sstream>

#include "GameHeader.h"
#include "Utils/StringUtils.h"


#ifdef EDITOR
bool AssetImporter::ImportAsset(const string & path)
{
	ID sourceID = ToHash(path.substr(game.assetsDir.size()));
	const string metaPath = path + ".meta";

	// get the right importer and load the meta file
	ImporterBase * importer = nullptr;
	Json::Value metaJson;
	if (!LoadMetaFile(metaPath, metaJson))
	{
		importer = GetDefaultImporter(path);
		if (!importer) { return false; }
		metaJson = importer->GetDefaultMetaJson();
	}
	else
	{
		importer = GetImporter(metaJson["importer"].asString());
	}

	// import the file and update imported asset data
	ImportData * importedAsset = GetImportData(sourceID);
	if (!importedAsset)
	{
		ImportData importData(sourceID, path);
		if (!importer->Import(path, metaJson, importData)) { return false; }
		importedAssets.push_back(importData);
		importedAsset = &importedAssets.back();
	}
	else
	{
		long long importTime = importedAsset->importTime;
		long long sourceLastWrite = GetLastWrite(path);
		long long metaLastWrite = GetLastWrite(metaPath);
		bool sourceUpToDate = importTime >= sourceLastWrite;
		bool metaUpToDate = importTime >= metaLastWrite;

		bool allFilesExist = true;
		for (const auto & ca : importedAsset->childDatas)
		{
			if (!fs::exists(StringUtils::ToPath(game.importsDir + to_string(ca.id))))
			{
				allFilesExist = false;
				break;
			}
		}

		if (sourceUpToDate && metaUpToDate && allFilesExist)
		{
			return true;
		}

		if (!importer->Import(path, metaJson, *importedAsset)) { return false; }
	}

	WriteMetaFile(metaPath, metaJson);
	importedAsset->importTime = time(nullptr);
	WriteImportedAssets();
	return true;
}

void AssetImporter::ImportAllAssets()
{
	vector<ID> existingIds;

	// import all assets
	for (auto & p : fs::recursive_directory_iterator(StringUtils::ToPath(game.assetsDir)))
	{
		if (!fs::is_regular_file(p)) { continue; }

		stringstream ss;
		ss << p;
		string path = ss.str();
		StringUtils::FixPath(path);


		if (path.find(".meta") != string::npos) { continue; }

		ImportAsset(path);
		existingIds.push_back(ToHash(path.substr(game.assetsDir.size())));
	}

	// delete unused meta files
	for (auto & p : fs::recursive_directory_iterator(StringUtils::ToPath(game.assetsDir)))
	{
		if (!fs::is_regular_file(p)) { continue; }

		stringstream ss;
		ss << p;
		string path = ss.str();
		StringUtils::FixPath(path);

		if (path.find(".meta") == string::npos) { continue; }

		string assetPath = path;
		assetPath.erase(assetPath.end() - 5, assetPath.end());
		if (fs::exists(StringUtils::ToPath(assetPath))) { continue; }
		fs::remove(StringUtils::ToPath(path));
	}

	// delete deleted AssetDatas
	for (int i = (int)importedAssets.size() - 1; i >= 0; i--)
	{
		auto & ia = importedAssets[i];
		bool deleted = true;
		for (ID id : existingIds)
		{
			if (ia.sourceID == id)
			{
				deleted = false;
				break;
			}
		}
		if (!deleted) { continue; }
		importedAssets.erase(importedAssets.begin() + i);
		WriteImportedAssets(); // todo make adding and erasing from imported assets into a function
	}

	// delete deleted assets
	for (auto & p : fs::recursive_directory_iterator(StringUtils::ToPath(game.importsDir)))
	{
		if (!fs::is_regular_file(p)) { continue; }

		stringstream ss;
		ss << p;
		string ps = ss.str();
		StringUtils::FixPath(ps);

		if (ps.find(".json") != string::npos) { continue; }
		ID id = stoull(ps.substr(ps.find_last_of('/') + 1));

		// todo: handle suffix
		bool deleted = true;
		for (auto & ia : importedAssets)
		{
			for (const auto & ca : ia.childDatas)
			{
				if (ca.id == id)
				{
					deleted = false;
					break;
				}
			}
		}
		if (!deleted) { continue; }
		fs::remove(p);
	}
}
#endif //EDITOR

ImportData * AssetImporter::GetImportData(ID sourceID)
{
	for (ImportData & importedAsset : importedAssets)
	{
		if (importedAsset.sourceID == sourceID)
		{
			return &importedAsset;
		}
	}
	return nullptr;
}

ImporterBase * AssetImporter::GetDefaultImporter(const string & path)
{
	string ending = StringUtils::GetFileEnding(path);
	for (ImporterBase * importer : importers)
	{
		for (const string & e : importer->GetEndings())
		{
			if (ending == e)
			{
				return importer;
			}
		}
	}

	//LOG("No Defaultimporter found: " + path);
	return nullptr;
}

ImporterBase * AssetImporter::GetImporter(const Name & name)
{
	for (ImporterBase * importer : importers)
	{
		if (importer->GetName() == name)
		{
			return importer;
		}
	}
	return nullptr;
}

bool AssetImporter::LoadMetaFile(const string & path, Json::Value & metaJson)
{
	ifstream metaFile;
	metaFile.open(path);
	if (metaFile.is_open())
	{
		string content = string(istreambuf_iterator<char>(metaFile), istreambuf_iterator<char>());
		if (content.empty())
		{
			LOG("empty meta file");
			return false;
		}

		Json::Reader reader;
		if (!reader.parse(content, metaJson))
		{
			LOG("meta file corrupted. couldnt read json");
			return false;
		}
	}
	else
	{
		return false;
	}
	metaFile.close();
	return true;
}

bool AssetImporter::WriteMetaFile(const string & path, const Json::Value & metaJson)
{
	bool success = false;
	ofstream metaFile;
	metaFile.open(path, ios::trunc | ios::out);
	if (metaFile.is_open())
	{
		Json::FastWriter writer;
		metaFile << writer.write(metaJson);
		success = true;
	}
	else
	{
		LOG("could not create meta file");
	}
	metaFile.close();
	return success;
}

void AssetImporter::ReadImportedAssets()
{
	Json::Value json;

	ifstream file(game.importsDir + importDataFileName);
	if (file.is_open())
	{
		string s;
		file >> s;
		Json::Reader reader;
		if (!reader.parse(s, json))
		{
			LOG_ERROR("Import data file corrupted");
		}
	}
	else
	{
		LOG("No Imported assets file");
	}

	for (auto & iaJson : json)
	{
		ImportData ia(iaJson["sourceID"].asUInt64(), iaJson["sourcePath"].asString());
		ia.importTime = iaJson["importTime"].asInt64();
		for (auto & cdJson : iaJson["childDatas"])
		{
			ia.childDatas
			  .push_back({ cdJson["id"].asUInt64(), cdJson["name"].asString(), cdJson["assetName"].asString() });
		}
		importedAssets.push_back(ia);
	}
}

void AssetImporter::WriteImportedAssets()
{
	Json::Value json;
	for (auto & ia : importedAssets)
	{
		Json::Value iaJson;
		iaJson["sourceID"] = (Json::UInt64)ia.sourceID;
		iaJson["sourcePath"] = ia.sourcePath;
		iaJson["importTime"] = (Json::Int64)ia.importTime;
		iaJson["childDatas"] = Json::Value();
		for (auto & cd : ia.childDatas)
		{
			Json::Value cdJson;
			cdJson["id"] = (Json::UInt64)cd.id;
			cdJson["name"] = cd.name;
			cdJson["assetName"] = cd.assetName.ToString();
			iaJson["childDatas"].append(cdJson);
		}
		json.append(iaJson);
	}
	ofstream file(game.importsDir + importDataFileName, ios::out | ios::trunc);
	if (file.is_open())
	{
		file << Json::FastWriter().write(json);
	}
	else
	{
		LOG_ERROR("failed to write imported asset data");
	}
	file.close();
}

long long AssetImporter::GetLastWrite(const string & path)
{
	auto lwC = fs::last_write_time(StringUtils::ToPath(path)).time_since_epoch();
	return std::chrono::duration_cast<std::chrono::seconds>(lwC).count();
}
