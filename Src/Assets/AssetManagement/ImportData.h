#pragma once

#include <vector>
#include <string>

#include "Game.h"

#include "Utils/Hash.h"


using namespace std;

struct ImportData
{
	ImportData(ID sourceID, const string & sourcePath) :
		sourceID(sourceID),
		sourcePath(sourcePath),
		importTime(0) {}

	ID sourceID;
	string sourcePath;
	long long importTime;

	struct ChildData
	{
		ChildData(const string & path, const Name & assetName, const string & name) :
			id(ToHash(path.substr(game.assetsDir.size()) + ":" + name)),
			name(name),
			assetName(assetName) {}

		ChildData(const string & path, const Name & assetName) :
			id(ToHash(path.substr(game.assetsDir.size()))),
			name(""),
			assetName(assetName) {}

		ChildData(ID id, const string & name, const Name & assetName) :
			id(id),
			name(name),
			assetName(assetName) {}

		ID id;
		string name;
		Name assetName;
	};

	vector<ChildData> childDatas;
};