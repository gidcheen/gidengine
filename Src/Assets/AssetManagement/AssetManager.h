#pragma once

#include <experimental/filesystem>
#include <string>

#include "Utils/Name.h"
#include "Utils/Logging.h"
#include "AssetPipeline.h"


using namespace std;
namespace fs = std::experimental::filesystem;

class AssetBase;


template<typename T>
class AssetRef;

class AssetManager
{
public:
private:
	vector<AssetBase *> loadedAssets;
	vector<AssetPipeline> pipelines;

public:
	AssetManager();
	~AssetManager();

	inline void AddAssetPipeline(const AssetPipeline & pipeline) { pipelines.push_back(pipeline); }

	template<typename T>
	AssetRef<T> GetAsset(ID id);
	template<typename T>
	AssetRef<T> GetAsset(string path);

	void IncreaseRefCnt(AssetBase * asset);
	void DecreaseRefCnt(AssetBase * asset);

private:
	AssetBase * GetOrLoadAsset(ID id);
	AssetBase * LoadAsset(ID id);
	AssetBase * GetLoadedAsset(ID id);

	const AssetPipeline * GetPipeline(const Name & name);
};

#include "AssetRef.h"


template<typename T>
inline AssetRef<T> AssetManager::GetAsset(ID id)
{
	T * loadedAsset = dynamic_cast<T *>(GetOrLoadAsset(id));
	if (!loadedAsset && id != 0)
	{
		LOG("Asset could not be loaded, id: " + to_string(id));
	}
	return AssetRef<T>(loadedAsset);
}

template<typename T>
inline AssetRef<T> AssetManager::GetAsset(string path)
{
	return GetAsset<T>(ToHash(path));
}
