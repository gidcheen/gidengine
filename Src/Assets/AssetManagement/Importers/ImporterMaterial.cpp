#include "ImporterMaterial.h"

#include "Assets/Material.h"


bool ImporterMaterial::Import(
	const string & path, const vector<char> & inContent, vector<OutContent> & outContent, const PropertyHolder & ph)
{
	outContent.push_back(OutContent(ImportData::ChildData(path, Material::GetMaterialName())));
	OutContent & outTexture = outContent.back();
	outTexture.data = inContent;

	return true;
}
