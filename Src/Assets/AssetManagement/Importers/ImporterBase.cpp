#include "ImporterBase.h"

#include "GameHeader.h"
#include "Utils/StringUtils.h"


bool ImporterBase::Import(const string & path, Json::Value & metaJson, ImportData & assetData)
{
	const string & importsDir = game.importsDir;
	if (!fs::exists(StringUtils::ToPath(importsDir)))
	{
		fs::create_directory(StringUtils::ToPath(importsDir));
	}

	// get the file to import content
	vector<char> inContent;
	vector<OutContent> outContents;

	ifstream inf(path, ios::binary);
	if (inf.is_open())
	{
		// get file size and reserve space in vector
		inf.seekg(0, ios::end);
		streampos fSize = inf.tellg();
		inf.seekg(0, ios::beg);
		inContent.reserve((unsigned long)fSize);
		inContent.insert(inContent.begin(), istreambuf_iterator<char>(inf), istreambuf_iterator<char>());

		if (inContent.size() == 0)
		{
			LOG("inContent size is 0, file reading failed or file is empty");
			inf.close();
			return false;
		}
	}
	else
	{
		LOG("Import file couldnt be opened");
		inf.close();
		return false;
	}
	inf.close();

	// fix meta json if it contains wrong properties
	Json::Value propJson = metaJson["properties"];
	auto & allP = properties.GetAll();
	if (allP.size() != metaJson.size())
	{
		propJson = properties.ToJson();
	}
	else
	{
		for (const auto p : allP)
		{
			bool exists = false;
			for (const auto pj : propJson)
			{
				// override if property is missing
				if (pj["name"].asString() == p->GetName())
				{
					// override if types arent equal
					PropertyBase prop(pj);
					if (*p->GetType() != *prop.GetType())
					{
						metaJson["properties"] = properties.ToJson();
						break;
					}
					exists = true;
					break;
				}
			}
			if (!exists)
			{
				metaJson["properties"] = properties.ToJson();
				break;
			}
		}
	}

	// actual import
	PropertyHolder ph;
	ph.SetFromJson(metaJson["properties"]);
	if (!Import(path, inContent, outContents, ph)) { return false; }
	ASSERT(outContents.size() > 0);

	// write imported data to file
	for (const auto & oc : outContents)
	{
		string outPath(game.importsDir + to_string(oc.childData.id));
		ofstream outf(outPath, ios::out | ios::trunc | ios::binary);
		if (outf.is_open())
		{
			outf.write(&oc.data[0], oc.data.size());
		}
		else
		{
			LOG("Could not write import to file: " + to_string(oc.childData.id));
			outf.close();
			return false;
		}
		outf.close();
	}

	for (const auto oc : outContents)
	{
		assetData.childDatas.push_back(oc.childData);
	}
	return true;
}

Json::Value ImporterBase::GetDefaultMetaJson()
{
	Json::Value json;
	json["importer"] = GetName().ToString();
	json["properties"] = properties.ToJson();
	return json;
}
