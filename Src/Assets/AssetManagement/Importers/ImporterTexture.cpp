#include "ImporterTexture.h"

#include "Assets/Texture.h"


bool ImporterTexture::Import(const string & path,
							 const vector<char> & inContent,
							 vector<OutContent> & outContent,
							 const PropertyHolder & ph)
{
	outContent.push_back(OutContent(ImportData::ChildData(path, Texture::GetTextureName())));
	OutContent & outTexture = outContent.back();

	Json::Value json;
	json["isCubeMap"] = *ph.Get(isCubeMap.GetName())->Get<bool>();

	string s = Json::FastWriter().write(json);
	outTexture.data = vector<char>(s.begin(), s.end());
	outTexture.data.push_back('\0');

	outTexture.data.insert(outTexture.data.end(), inContent.begin(), inContent.end());

	return true;;
}
