#pragma once

#include "ImporterBase.h"


class ImporterTexture : public ImporterBase
{
NAME(ImporterTexture)
private:
	PBool isCubeMap;

public:
	ImporterTexture() :
		isCubeMap(properties, false, "isCubeMap") {}

	virtual vector<string> GetEndings() { return { "png", "jpg", "jpeg", "psd", "tif" }; }

protected:
	virtual bool Import(const string & path,
						const vector<char> & inContent,
						vector<OutContent> & outContent,
						const PropertyHolder & ph) override;
};

