#pragma once

#include "ImporterBase.h"


class ImporterMaterial : public ImporterBase
{
NAME(ImpoerterMaterial)
public:
	virtual vector<string> GetEndings() { return { "material" }; }

protected:
	virtual bool Import(const string & path, const vector<char> & inContent, vector<OutContent> & outContent,
						const PropertyHolder & ph) override;
};
