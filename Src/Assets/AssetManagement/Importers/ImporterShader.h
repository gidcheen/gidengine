#pragma once

#include "ImporterBase.h"


class ImporterShader : public ImporterBase
{
NAME(ImporterShader)
public:
	virtual vector<string> GetEndings() override { return { "shader" }; };

protected:
	virtual bool Import(const string & path, const vector<char> & inContent, vector<OutContent> & outContent,
						const PropertyHolder & ph) override;
};
