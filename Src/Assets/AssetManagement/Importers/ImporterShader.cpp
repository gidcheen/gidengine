#include "ImporterShader.h"

#include "Assets/Shader.h"
#include "Utils/Logging.h"


bool ImporterShader::Import(const string & path, const vector<char> & inContent, vector<OutContent> & outContent,
							const PropertyHolder & ph)
{
	outContent.push_back(ImportData::ChildData(path, Shader::GetShaderName()));
	OutContent & outShader = outContent.back();
	outShader.data = inContent;
	return true;
}
