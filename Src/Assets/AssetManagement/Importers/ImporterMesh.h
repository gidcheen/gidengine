#pragma once

#include <string>
#include <vector>
#include <assimp/mesh.h>
#include <assimp/scene.h>

#include "ImporterBase.h"


using namespace std;

class ImporterMesh : public ImporterBase
{
NAME(ImporterMesh)
public:
	vector<string> GetEndings() override { return { "dae", "3ds" }; }

protected:
	bool Import(
		const string & path,
		const vector<char> & inContent,
		vector<OutContent> & outContent,
		const PropertyHolder & ph) override;

private:
	void AppendChildMesh(
		Json::Value & data,
		const aiScene * scene,
		const aiNode * node,
		const Mat4 & parentTransform,
		unsigned int & vertexOffset) const;

	void AppendVerticesFromMesh(aiMesh * mesh, const Mat4 & transform, Json::Value & verticesJson) const;
	void AppendIndicesFromMesh(aiMesh * mesh, Json::Value & verticesJson, unsigned int vertexOffset) const;

	Mat4 AiMatToMat4(const aiMatrix4x4 src) const { return Mat4(src[0]).Transpose(); }
};
