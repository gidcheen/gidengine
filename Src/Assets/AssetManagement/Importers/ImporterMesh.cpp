#include "ImporterMesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Assets/Mesh.h"

#include "Maths/Maths.h"
#include "Utils/Logging.h"
#include "Utils/Assert.h"


using namespace Assimp;


bool ImporterMesh::Import(
	const string & path, const vector<char> & inContent, vector<OutContent> & outContent, const PropertyHolder & ph)
{
	outContent.push_back(OutContent(ImportData::ChildData(path, Mesh::GetMeshName())));
	OutContent & meshOut = outContent.back();

	Importer importer;

	const aiScene * scene = importer.ReadFileFromMemory(
		&inContent[0],
		inContent.size(),
		aiProcess_CalcTangentSpace | aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices | aiProcess_SortByPType |
		aiProcess_FlipUVs);

	if (scene == nullptr)
	{
		LOG_ERROR("assimp failed");
		LOG_ERROR(importer.GetErrorString());
		return false;
	}

	Json::Value data;
	unsigned int vertexOffset = 0;


	AppendChildMesh(data, scene, scene->mRootNode, Mat4::Identity(), vertexOffset);


	Json::FastWriter writer;
	string outContentStr = writer.write(data);
	meshOut.data.insert(meshOut.data.begin(), outContentStr.begin(), outContentStr.end());

	return true;
}

void ImporterMesh::AppendChildMesh(
	Json::Value & data,
	const aiScene * scene,
	const aiNode * node,
	const Mat4 & parentTransform,
	unsigned int & vertexOffset) const
{
	Mat4 transform = parentTransform * AiMatToMat4(node->mTransformation);
	for (int i = 0; i < node->mNumMeshes; i++)
	{
		AppendVerticesFromMesh(scene->mMeshes[node->mMeshes[i]], transform, data["v"]);
		AppendIndicesFromMesh(scene->mMeshes[node->mMeshes[i]], data["i"], vertexOffset);
		vertexOffset = data["v"].size();
	}

	for (int i = 0; i < node->mNumChildren; i++)
	{
		AppendChildMesh(data, scene, node->mChildren[i], transform, vertexOffset);
	}
}

void ImporterMesh::AppendVerticesFromMesh(aiMesh * mesh, const Mat4 & transform, Json::Value & verticesJson) const
{
	for (int i = 0; i < mesh->mNumVertices; i++)
	{
		Json::Value vertJson;

		Vec4 position(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z, 1);
		position = transform * position;
		vertJson["position"]["x"] = position.x;
		vertJson["position"]["y"] = position.y;
		vertJson["position"]["z"] = position.z;

		Mat4 normalMat = transform.Inverse().Transposed();
		Vec4 normal(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z, 0);
		normal = normalMat * normal;
		vertJson["normal"]["x"] = normal.x;
		vertJson["normal"]["y"] = normal.y;
		vertJson["normal"]["z"] = normal.z;

		Vec4 tangent(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z, 0);
		tangent = normalMat * tangent;
		vertJson["tangent"]["x"] = tangent.x;
		vertJson["tangent"]["y"] = tangent.y;
		vertJson["tangent"]["z"] = tangent.z;

		Vec4 bitangent(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z, 0);
		bitangent = normalMat * bitangent;
		vertJson["bitangent"]["x"] = bitangent.x;
		vertJson["bitangent"]["y"] = bitangent.y;
		vertJson["bitangent"]["z"] = bitangent.z;

		if (mesh->mColors && mesh->mColors[0])
		{
			vertJson["color"]["x"] = mesh->mColors[0][i].r;
			vertJson["color"]["y"] = mesh->mColors[0][i].g;
			vertJson["color"]["z"] = mesh->mColors[0][i].b;
		}

		if (mesh->mTextureCoords && mesh->mTextureCoords[0])
		{
			vertJson["uv"]["u"] = mesh->mTextureCoords[0][i].x;
			vertJson["uv"]["v"] = mesh->mTextureCoords[0][i].y;
		}

		verticesJson.append(vertJson);
	}
}

void ImporterMesh::AppendIndicesFromMesh(aiMesh * mesh, Json::Value & verticesJson, unsigned int vertexOffset) const
{
	for (int i = 0; i < mesh->mNumFaces; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			verticesJson[mesh->mMaterialIndex].append(mesh->mFaces[i].mIndices[j] + vertexOffset);
		}
	}
}
