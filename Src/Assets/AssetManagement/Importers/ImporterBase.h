#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <json/json.h>
#include <experimental/filesystem>

#include "Utils/Named.h"
#include "Assets/AssetBase.h"
#include "PropertySystem/PropertySystem.h"
#include "Assets/AssetManagement/ImportData.h"


using namespace std;
namespace fs = std::experimental::filesystem;

struct OutContent
{
	OutContent(const ImportData::ChildData & childData) :
		childData(childData) {}
	vector<char> data;
	ImportData::ChildData childData;
};

class ImporterBase : public Named
{
protected:
	PropertyHolder properties;

private:
	int importance;

public:
	ImporterBase() :
		importance(0) {}
	virtual ~ImporterBase() {};

	inline int GetImportance() { return importance; }

	virtual vector<string> GetEndings() = 0;

	bool Import(const string & path, Json::Value & metaJson, ImportData & assetData);

	Json::Value GetDefaultMetaJson();

protected:
	virtual bool Import(
		const string & path,
		const vector<char> & inContent,
		vector<OutContent> & outContent,
		const PropertyHolder & ph) = 0;
};
