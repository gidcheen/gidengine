#pragma once

#include <string>

#include "Game.h"
#include "AssetManager.h"
#include "Utils/Hash.h"


using namespace std;

template<typename T>
class AssetRef
{
private:
	T * asset;

public:
	AssetRef() :
		asset(nullptr) {}

	AssetRef(ID id) :
		asset(nullptr) { SetAsset(id); }

	AssetRef(const string & name) :
		asset(nullptr) { SetAsset(name); }

	AssetRef(const char * name) :
		AssetRef(string(name)) {}

	AssetRef(T * asset) :
		asset(nullptr) { SetAsset(asset); }

	AssetRef(const AssetRef<T> & other) :
		asset(nullptr) { SetAsset(other.GetAsset()); }

	~AssetRef() { ReleaseAsset(); }

	AssetRef<T> & operator=(const AssetRef<T> & other) { return SetAsset(other.GetAsset()); }
	AssetRef<T> & operator=(ID id) { return SetAsset(id); }
	AssetRef<T> & operator=(const string & name) { return SetAsset(name); }
	AssetRef<T> & operator=(T * asset) { return SetAsset(asset); }

	T * operator->() { return asset; }
	const T * operator->() const { return asset; }

	inline ID GetId() { return asset ? asset->GetId() : 0; }

	operator T *() { return asset; }
	operator const T *() { return asset; }
	T * GetAsset() const { return asset; }

	AssetRef<T> & SetAsset(ID id) { return *this = game.assetManager->GetAsset<T>(id); }
	AssetRef<T> & SetAsset(const string & name) { return *this = game.assetManager->GetAsset<T>(name); }
	AssetRef<T> & SetAsset(T * asset)
	{
		if (asset)
		{
			ReleaseAsset();
			this->asset = asset;
			IncreaseRefCnt();
		}
		return *this;
	}

	inline bool IsEmpty() const { return !asset; }

private:
	void IncreaseRefCnt() { game.assetManager->IncreaseRefCnt(asset); }
	void ReleaseAsset() { game.assetManager->DecreaseRefCnt(asset); }
};
