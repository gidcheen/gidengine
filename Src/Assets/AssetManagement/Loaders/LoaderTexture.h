#pragma once

#include "LoaderBase.h"


class LoaderTexture : public LoaderBase
{
protected:
	virtual AssetBase * Load(const vector<char> & content) const override;
};

