#pragma once

#include "LoaderBase.h"


class LoaderShader : public LoaderBase
{
protected:
	virtual AssetBase * Load(const vector<char> & content) const override;
};
