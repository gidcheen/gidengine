#include <Utils/StringUtils.h>
#include "LoaderMaterial.h"

#include "Assets/Material.h"
#include "Utils/StringUtils.h"


AssetBase * LoaderMaterial::Load(const vector<char> & content) const
{
	vector<string> ss = StringUtils::SplitString(string(content.begin(), content.end()), '\n');

	string shaderName = ss[0];
	string transparency = ss[1];

	Material::Transparency t = Material::opaque;
	if (transparency == "cutout")
	{
		t = Material::cutout;
	}
	else if (transparency == "translucent")
	{
		t = Material::translucent;
	}

	Material * mat = new Material(shaderName, t);

	for (int i = 2; i < ss.size(); i++)
	{
		mat->SetProperty(StringUtils::CreateProperty(ss[i]));
	}

	return mat;
}
