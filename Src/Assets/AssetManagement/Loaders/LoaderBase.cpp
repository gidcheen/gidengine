#include "LoaderBase.h"

#include <experimental/filesystem>

#include "GameHeader.h"
#include "Utils/StringUtils.h"


namespace fs = std::experimental::filesystem;

AssetBase * LoaderBase::Load(ID id) const
{
	const string & importsDir = game.importsDir;
	if (!fs::exists(StringUtils::ToPath(importsDir)))
	{
		LOG("path for loading asset doesnt exist. No imported assets?");
		return nullptr;
	}

	string path = importsDir + to_string(id);

	vector<char> content;

	ifstream f(path, ios::binary);
	if (f.is_open())
	{
		// get file size and reserve space in vector
		vector<char> content;
		f.seekg(0, ios::end);
		streampos fSize = f.tellg();
		f.seekg(0, ios::beg);
		content.reserve(fSize);
		content.insert(content.begin(), istreambuf_iterator<char>(f), istreambuf_iterator<char>());

		AssetBase * asset = Load(content);
		f.close();
		return asset;
	}
	else
	{
		LOG("Could not write importet model to file");
		f.close();
		return nullptr;
	}
}
