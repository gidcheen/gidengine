#pragma once

#include "LoaderBase.h"


class LoaderMesh : public LoaderBase
{
protected:
	AssetBase * Load(const vector<char> & content) const override;
};
