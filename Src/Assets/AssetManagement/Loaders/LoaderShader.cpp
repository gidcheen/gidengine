#include "LoaderShader.h"

#include "Assets/Shader.h"
#include "Utils/StringUtils.h"


AssetBase * LoaderShader::Load(const vector<char> & content) const
{
	string contentStr(content.begin(), content.end());
	vector<string> strings = StringUtils::SplitString(contentStr, "<---->");

	GLShader::ShaderSrcs srcs = {};
	vector<PropertyBase *> defaults;
	string newlines;

	ASSERT(strings.size() % 2 == 0);
	for (int i = 0; i < strings.size(); i += 2)
	{
		const string type = StringUtils::SplitString(strings[i], ' ')[0];
		const string & value = strings[i + 1];

		if (type == "defaults")
		{
			for (const string & line : StringUtils::SplitString(value, '\n'))
			{
				defaults.push_back(StringUtils::CreateProperty(line));
			}
		}
		else if (type == "vert")
		{
			srcs.vertSrc = newlines + value;
		}
		else if (type == "tess")
		{
			srcs.tessSrc = newlines + value;
		}
		else if (type == "geom")
		{
			srcs.geomSrc = newlines + value;
		}
		else if (type == "frag")
		{
			srcs.fragSrc = newlines + value;
		}

		long newlinesInValue = count(value.begin(), value.end(), '\n');
		for (int j = 0; j < newlinesInValue; j++)
		{
			newlines.push_back('\n');
		}
	}

	return new Shader(srcs, defaults);
}
