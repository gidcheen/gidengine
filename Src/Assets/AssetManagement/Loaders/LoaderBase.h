#pragma once

#include <vector>
#include "Utils/Hash.h"

using namespace std;

class AssetBase;

class LoaderBase
{
public:
	AssetBase * Load(ID id) const;

protected:
	virtual AssetBase * Load(const vector<char> & content) const = 0;
};
