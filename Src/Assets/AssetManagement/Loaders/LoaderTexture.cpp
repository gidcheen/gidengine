#include "LoaderTexture.h"

#include <algorithm>
#include <json/json.h>


#define STB_IMAGE_IMPLEMENTATION

#include "_External/stb_image.h"
#include "Assets/Texture.h"


AssetBase * LoaderTexture::Load(const vector<char> & content) const
{
	Json::Value json;
	Json::Reader reader;
	auto it = find(content.begin(), content.end(), '\0');
	string s(content.begin(), it);
	if (!reader.parse(s, json))
	{
		LOG("Texture corrupted properties");
		return nullptr;
	}

	long dataBegin = distance(content.begin(), it + 1);
	int width;
	int height;
	int nrChannels;
	unsigned char * stbi_data = stbi_load_from_memory(
		(unsigned char *)content.data() + dataBegin, int(content.size() - dataBegin), &width, &height, &nrChannels, 0);

	if (stbi_data == nullptr)
	{
		LOG("image could not be loaded");
		return nullptr;
	}

	// memcopy. probably not the best idea
	unsigned char * data = new unsigned char[width * height * nrChannels]; // todo: mem leak
	memcpy(data, stbi_data, (size_t)width * height * nrChannels);

	stbi_image_free(stbi_data);

	if (json["isCubeMap"].asBool())
	{
		return new Texture(data, width, height, nrChannels, true, Wrapping::clamp, Filtering::linear);
	}
	return new Texture(data, width, height, nrChannels, false, Wrapping::repeat, Filtering::linear);
}
