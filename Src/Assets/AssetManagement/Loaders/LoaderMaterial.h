#pragma once

#include "LoaderBase.h"


class LoaderMaterial : public LoaderBase
{
protected:
	virtual AssetBase * Load(const vector<char> & content) const;
};
