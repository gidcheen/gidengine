#include "LoaderMesh.h"

#include <json/json.h>

#include "Assets/Mesh.h"
#include "Graphics/Vertex.h"
#include "Assets/Material.h"


AssetBase * LoaderMesh::Load(const vector<char> & content) const
{
	Json::Reader reader;
	Json::Value json;

	if (!reader.parse(string(content.begin(), content.end()), json))
	{
		return nullptr;
	}

	unsigned int vc = json["v"].size();
	vector<unsigned int> ics(json["i"].size());
	for (int i = 0; i < ics.size(); i++)
	{
		ics[i] = json["i"][i].size();
	}

	Vertex * vertices;
	vertices = new Vertex[vc];
	vector<unsigned int *> indices;
	for (int i = 0; i < ics.size(); i++)
	{
		indices.push_back(new unsigned int[ics[i]]);
	}

	Json::Value vertJson = json.get("v", Json::Value());
	for (int i = 0; i < vc; i++)
	{
		Json::Value positionJson = vertJson[i]["position"];
		vertices[i].position.x = positionJson["x"].asFloat();
		vertices[i].position.y = positionJson["y"].asFloat();
		vertices[i].position.z = positionJson["z"].asFloat();

		Json::Value normalJson = vertJson[i].get("normal", Json::Value());
		vertices[i].normal.x = normalJson["x"].asFloat();
		vertices[i].normal.y = normalJson["y"].asFloat();
		vertices[i].normal.z = normalJson["z"].asFloat();

		Json::Value tangentJson = vertJson[i].get("tangent", Json::Value());
		vertices[i].tangent.x = tangentJson["x"].asFloat();
		vertices[i].tangent.y = tangentJson["y"].asFloat();
		vertices[i].tangent.z = tangentJson["z"].asFloat();

		Json::Value bitangentJson = vertJson[i].get("bitangent", Json::Value());
		vertices[i].bitangent.x = bitangentJson["x"].asFloat();
		vertices[i].bitangent.y = bitangentJson["y"].asFloat();
		vertices[i].bitangent.z = bitangentJson["z"].asFloat();

		Json::Value colorJson = vertJson[i].get("color", Json::Value());
		vertices[i].color.x = colorJson.get("x", 1.f).asFloat();
		vertices[i].color.y = colorJson.get("y", 1.f).asFloat();
		vertices[i].color.z = colorJson.get("z", 1.f).asFloat();

		Json::Value uvJson = vertJson[i].get("uv", Json::Value());
		vertices[i].uv.x = uvJson.get("u", 0).asFloat();
		vertices[i].uv.y = uvJson.get("v", 0).asFloat();
	}

	vector<unsigned  int> yo;
	Json::Value indexJson = json["i"];
	for (int i = 0; i < ics.size(); i++)
	{
		for (int j = 0; j < ics[i]; j++)
		{
			indices[i][j] = indexJson[i][j].asUInt();
			yo.push_back(indexJson[i][j].asUInt());
		}
	}

	// temp
	return new Mesh(MeshData(vertices, vc, indices, ics));
}
