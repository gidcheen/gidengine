#pragma once

#include <experimental/filesystem>
#include <string>
#include <algorithm>

#include "Assets/AssetManagement/Importers/ImporterBase.h"
#include "ImportData.h"


using namespace std;

class AssetImporter
{
private:
	const string importDataFileName = "importData.json";

	vector<ImportData> importedAssets;
	vector<ImporterBase *> importers;

public:
	AssetImporter() { ReadImportedAssets(); }

	inline const vector<ImportData> & GetImportedAssetDatas() { return importedAssets; }

#ifdef EDITOR
	bool ImportAsset(const string & path);
	void ImportAllAssets();
#endif //EDITOR

	inline void AddImporter(ImporterBase * importerBase)
	{
		importers.push_back(importerBase);
		sort(importers.begin(), importers.end(), [](ImporterBase * a, ImporterBase * b)
		{ return a->GetImportance() > b->GetImportance(); });
	}

private:
	ImportData * GetImportData(ID sourceID);

	ImporterBase * GetDefaultImporter(const string & path);
	ImporterBase * GetImporter(const Name & name);

	bool LoadMetaFile(const string & path, Json::Value & metaJson);
	bool WriteMetaFile(const string & path, const Json::Value & metaJson);

	void ReadImportedAssets();
	void WriteImportedAssets();

	long long GetLastWrite(const string & path);
};
