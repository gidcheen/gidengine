#pragma once

#include <vector>
#include <string>

#include "Assets/AssetManagement/Loaders/LoaderBase.h"
#include "Utils/Name.h"


class AssetPipeline
{
public:
	const Name name;
	const LoaderBase * loader;
	// todo: editor
	// todo: preview
	// todo: thumbnail generator

public:
	AssetPipeline(const Name & name, const LoaderBase * loader) :
		name(name),
		loader(loader) {}

	~AssetPipeline()
	{
		//delete loader;
	};
};
