#include "AssetManager.h"

#include <algorithm>
#include <json/json.h>

#include "GameHeader.h"
#include "Assets/AssetBase.h"
#include "Assets/AssetManagement/Loaders/LoaderBase.h"

#include "AssetRef.h"


AssetManager::AssetManager()
{
}

AssetManager::~AssetManager()
{
}

void AssetManager::IncreaseRefCnt(AssetBase * asset)
{
	if (!asset->isRefCounted) { return; }
	asset->refCnt++;
}

void AssetManager::DecreaseRefCnt(AssetBase * asset)
{
	if (!asset || !asset->isRefCounted) { return; }
	asset->refCnt--;

	ASSERT(asset->refCnt >= 0);
	if (asset->refCnt == 0)
	{
		for (int i = 0; i <= loadedAssets.size(); i++)
		{
			if (loadedAssets[i] == asset)
			{
				delete asset;
				loadedAssets.erase(loadedAssets.begin() + i);
			}
		}
	}
}

AssetBase * AssetManager::GetOrLoadAsset(ID id)
{
	if (id == 0) { return nullptr; }
	AssetBase * asset = GetLoadedAsset(id);
	if (!asset) { asset = LoadAsset(id); }
	return asset;
}

AssetBase * AssetManager::LoadAsset(ID id)
{
	const LoaderBase * loader = nullptr;
	for (auto & iad : game.assetImporter->GetImportedAssetDatas())
	{
		if (loader) { break; }
		for (auto & ca : iad.childDatas)
		{
			if (ca.id != id) { continue; }
			const AssetPipeline * pipeline = GetPipeline(ca.assetName);
			ASSERT(pipeline);
			loader = pipeline->loader;
			break;
		}
	}

	if (!loader)
	{
		LOG_ERROR("failed to get loader when loading assets");
		ASSERT(false);
		return nullptr;
	}

	AssetBase * asset = loader->Load(id);
	if (!asset) { return nullptr; }

	asset->isRefCounted = true;
	asset->id = id;
	loadedAssets.push_back(asset);

	return asset;
}

AssetBase * AssetManager::GetLoadedAsset(ID id)
{
	for (size_t i = 0; i < loadedAssets.size(); i++)
	{
		if (loadedAssets[i]->GetId() == id)
		{
			return loadedAssets[i];
		}
	}
	return nullptr;
}

const AssetPipeline * AssetManager::GetPipeline(const Name & name)
{
	for (const AssetPipeline & p : pipelines)
	{
		if (p.name == name)
		{
			return &p;
		}
	}
	return nullptr;
}
