#include "Scene.h"

#include <algorithm>
#include <fstream>

#include "Entity.h"
#include "Component.h"


Scene::Scene() :
	rootEntity(nullptr)
{
}

Scene::~Scene()
{
	delete rootEntity;
}

void Scene::Awake()
{
	auto to = toAwake;
	toAwake.clear();
	for (Component * c : to)
	{
		c->Awake();
	}
}

void Scene::Destroy()
{
	auto to = toDestroy;
	toDestroy.clear();
	for (Component * c : to)
	{
		c->Destroy();
		toStart.erase(remove(toStart.begin(), toStart.end(), c), toStart.end());
		toStop.erase(remove(toStop.begin(), toStop.end(), c), toStop.end());
		toUpdate.erase(remove(toUpdate.begin(), toUpdate.end(), c), toUpdate.end());
		toPrePhysics.erase(remove(toPrePhysics.begin(), toPrePhysics.end(), c), toPrePhysics.end());
		toPostPhysics.erase(remove(toPostPhysics.begin(), toPostPhysics.end(), c), toPostPhysics.end());
		toRender.erase(remove(toRender.begin(), toRender.end(), c), toRender.end());
		delete c;
	}
	auto eto = entitiesToDestroy;
	entitiesToDestroy.clear();
	for (Entity * e : eto)
	{
		entities.erase(remove(entities.begin(), entities.end(), e), entities.end());
		// should be done by the delete.
		// the Delete() of the component still has access to the parent but the parent doesnt see the entity as a child anymore
		//if (e->parent) { e->parent->RemoveChild(e); } //
		delete e;
	}
}

void Scene::Start()
{
	auto to = toStart;
	toStart.clear();
	for (Component * c : to)
	{
		if (!CanUpdate(c)) { continue; }
		c->Start();
	}
}

void Scene::Stop()
{
	auto to = toStop;
	toStop.clear();
	for (Component * c : to)
	{
		if (!CanUpdate(c)) { continue; }
		c->Stop();
	}
}

void Scene::Update(double dt)
{
	auto to = toUpdate;
	for (Component * c : to)
	{
		if (!CanUpdate(c)) { continue; }
		c->Update(dt);
	}
}

void Scene::PrePhysics()
{
	auto to = toPrePhysics;
	for (Component * c : to)
	{
		if (!CanUpdate(c)) { continue; }
		c->PrePhysicsUpdate();
	}
}

void Scene::PostPhysics()
{
	auto to = toPostPhysics;
	for (Component * c : to)
	{
		if (!CanUpdate(c)) { continue; }
		c->PostPhysicsUpdate();
	}
}

void Scene::Render()
{
	auto to = toRender;
	for (Component * c : to)
	{
		if (!CanUpdate(c)) { continue; }
		c->RenderUpdate();
	}
}

void Scene::RemoveFromUpdate(Component * c)
{
	toUpdate.erase(remove(toUpdate.begin(), toUpdate.end(), c), toUpdate.end());
}

void Scene::RemoveFromPrePhysics(Component * c)
{
	toPrePhysics.erase(remove(toPrePhysics.begin(), toPrePhysics.end(), c), toPrePhysics.end());
}

void Scene::RemoveFromPostPhysics(Component * c)
{
	toPostPhysics.erase(remove(toPostPhysics.begin(), toPostPhysics.end(), c), toPostPhysics.end());
}

void Scene::RemoveFromRender(Component * c)
{
	toRender.erase(remove(toRender.begin(), toRender.end(), c), toRender.end());
}

void Scene::SaveScene(const string & path)
{
	SaveScene(rootEntity, path);
}

void Scene::SaveScene(const Entity * root, const string & path)
{
	ofstream outFile(path, ios::out | ios::trunc);
	if (outFile.is_open())
	{
		outFile << Json::FastWriter().write(root->ToJson());
	}
	outFile.close();
}

bool Scene::LoadScene(const string & path)
{
	DeleteEntity(rootEntity);
	return LoadScene(rootEntity, path);
}

bool Scene::LoadScene(Entity * root, const string & path)
{
	ifstream f(path);
	if (f.is_open())
	{
		Json::Value json;
		Json::Reader reader;
		if (!reader.parse(f, json))
		{
			LOG_ERROR("Scene file corrupt: " + path);
			return false;
		}

		Entity * e = new Entity(json, root);
		LOG("" + e->name);
	}
	else
	{
		LOG_ERROR("Scene faild to load: " + path);
		return false;
	}
	f.close();
	return true;
}

void Scene::DeleteEntity(Entity * entity)
{
	ASSERT(entity);
	if (entity->IsRoot())
	{
		rootEntity = nullptr;
	}
	else
	{
		entity->GetParent()->RemoveChild(entity);
	}
	DestroyEntity(entity);
}

void Scene::DestroyEntity(Entity * entity)
{
	entitiesToDestroy.push_back(entity);
	for (Entity * e : entity->GetChildren())
	{
		DestroyEntity(e);
	}
	for (Component * c : entity->GetComponents())
	{
		DestroyComponent(c);
	}
}

void Scene::DestroyComponent(Component * c)
{
	toDestroy.push_back(c);
}

bool Scene::CanUpdate(Component * c)
{
	return c->GetIsActive() && !c->GetMarkedForDelete();
}
