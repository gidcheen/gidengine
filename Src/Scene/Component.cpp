#include "Component.h"

#include "GameHeader.h"


Component::Component(Entity * entity, const Json::Value & json) :
	entity(entity),
	isActive(true),
	receivesUpdate(false),
	receivesPrePhysics(false),
	receivesPostPhysics(false),
	receivesRender(false),
	markedForDelete(false)
{
	entity->AddComponent(this);
	game.scene->AddToAwake(this);
	game.scene->AddToStart(this);
	SetFromJson(json);
}

void Component::SetIsActive(bool ia)
{
	if (ia && !isActive) { game.scene->AddToStart(this); }
	else if (!ia && isActive) { game.scene->AddToStop(this); }
	isActive = ia;
}

void Component::SetReceivesUpdate(bool r)
{
	if (r == receivesUpdate) { return; }
	if (r)
	{
		game.scene->AddToUpdate(this);
	}
	else
	{
		game.scene->RemoveFromUpdate(this);
	}
}

void Component::SetReceivesPrePhysics(bool r)
{
	if (r == receivesPrePhysics) { return; }
	if (r)
	{
		game.scene->AddToPrePhysics(this);
	}
	else
	{
		game.scene->RemoveFromPrePhysics(this);
	}
}

void Component::SetReceivesPostPhysics(bool r)
{
	if (r == receivesPostPhysics) { return; }
	if (r)
	{
		game.scene->AddToPostPhysics(this);
	}
	else
	{
		game.scene->RemoveFromPostPhysics(this);
	}
}

void Component::SetReceivesRender(bool r)
{
	if (r == receivesRender) { return; }
	if (r)
	{
		game.scene->AddToRender(this);
	}
	else
	{
		game.scene->RemoveFromRender(this);
	}
}

Json::Value Component::ToJson() const
{
	Json::Value json;
	json["name"] = GetName().ToString();
	json["isActive"] = isActive;
	json["receivesUpdate"] = receivesUpdate;
	json["receivesPrePhysics"] = receivesPrePhysics;
	json["receivesPostPhysics"] = receivesPostPhysics;
	json["receivesRender"] = receivesRender;

	json["properties"] = properties.ToJson();

	return json;
}

void Component::SetFromJson(const Json::Value & json)
{
	if (json.isNull()) { return; }
	SetIsActive(json["isActive"].asBool());
	SetReceivesUpdate(json["receivesUpdate"].asBool());
	SetReceivesPrePhysics(json["receivesPrePhysics"].asBool());
	SetReceivesPostPhysics(json["receivesPostPhysics"].asBool());
	SetReceivesRender(json["receivesRender"].asBool());

	properties.SetFromJson(json["properties"]);
}
