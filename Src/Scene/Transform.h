#pragma once

#include <json/json.h>
#include "Maths/Maths.h"


class Entity;

class Transform
{
private:
	Entity * entity;

	Vec3 position;
	Vec3 rotation;
	Vec3 scale;

	bool isDirty;
	bool isInverseDirty;
	Mat4 localToWorldMatrix;
	Mat4 worldToLocalMatrix;

public:
	explicit Transform(Entity * entity);

	Transform(const Transform &) = delete;
	Transform & operator=(const Transform &) = delete;

	// local
	const Vec3 & GetPosition() const { return position; }
	void SetPosition(const Vec3 & position);

	const Vec3 & GetRotation() const { return rotation; }
	void SetRotation(const Vec3 & rotation);

	const Vec3 & GetScale() const { return scale; }
	void SetScale(const Vec3 & scale);

	// global
	Vec3 GetGlobalPosition() { return Vec3(GetLocalToWorldMatrix() * Vec4(0, 0, 0, 1)); }
	void SetGlobalPosition(const Vec3 & position);

	Vec3 GetGlobalRotation() const;
	void SetGlobalRotation(const Vec3 & rotation);

	void Translate(Vec3 delta) { SetPosition(GetPosition() + delta); }
	void Rotate(Vec3 delta) { SetRotation(GetRotation() + delta); }
	void Scale(Vec3 delta) { SetScale(GetScale() + delta); }

	Vec3 GetForward() const { return Vec3(Mat4::Rotation(rotation).rawColumns[2]); }
	Vec3 GetRight() const { return Vec3(Mat4::Rotation(rotation).rawColumns[0]); }
	Vec3 GetUp() const { return Vec3(Mat4::Rotation(rotation).rawColumns[1]); }

	Vec3 GetGlobalForward() const;
	Vec3 GetGlobalRight() const;
	Vec3 GetGlobalUp() const;


	Mat4 GetLocalToWorldMatrix();
	Mat4 GetWorldToLocalMatrix();

	Json::Value ToJson() const;
	void SetFromJson(const Json::Value & json);

private:
	Mat4 CalculateLocalToParentMatrix() { return Mat4::Transform(position, rotation, scale); }
	void SetDirty();
};
