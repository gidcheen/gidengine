#include "Transform.h"

#include "Entity.h"
#include "GameHeader.h"


Transform::Transform(Entity * entity) :
	entity(entity),
	scale(1, 1, 1),
	isDirty(true),
	isInverseDirty(true),
	localToWorldMatrix(Mat4::Identity()),
	worldToLocalMatrix(Mat4::Identity())
{
}

void Transform::SetPosition(const Vec3 & position)
{
	this->position = position;
	SetDirty();
}

void Transform::SetRotation(const Vec3 & rotation)
{
	this->rotation = rotation;
	SetDirty();
}

void Transform::SetScale(const Vec3 & scale)
{
	this->scale = scale;
	SetDirty();
}

void Transform::SetGlobalPosition(const Vec3 & position)
{
	Vec3 localPos(entity->GetParent()->transform.GetWorldToLocalMatrix() * Vec4(position, 1));
	SetPosition(localPos);
}

Vec3 Transform::GetGlobalRotation() const
{
	Mat4 globalRot = entity->GetParent()->transform.GetLocalToWorldMatrix() * Mat4::Rotation(rotation);
	return globalRot.ToRotation();
}

void Transform::SetGlobalRotation(const Vec3 & rotation)
{
	Mat4 globalRot = entity->GetParent()->transform.GetWorldToLocalMatrix() * Mat4::Rotation(rotation);
	SetRotation(globalRot.ToRotation());
}

Vec3 Transform::GetGlobalForward() const
{
	Mat4 globalRot = entity->GetParent()->transform.GetLocalToWorldMatrix() * Mat4::Rotation(rotation);
	return Vec3(globalRot.rawColumns[2]); // not sure if normalize is necessary
}

Vec3 Transform::GetGlobalRight() const
{
	Mat4 globalRot = entity->GetParent()->transform.GetLocalToWorldMatrix() * Mat4::Rotation(rotation);
	return Vec3(globalRot.rawColumns[0]).Normalize();
}

Vec3 Transform::GetGlobalUp() const
{
	Mat4 globalRot = entity->GetParent()->transform.GetLocalToWorldMatrix() * Mat4::Rotation(rotation);
	return Vec3(globalRot.rawColumns[1]).Normalize();
}

Mat4 Transform::GetLocalToWorldMatrix()
{
	if (isDirty)
	{
		isDirty = false;
		if (entity->GetParent())
		{
			localToWorldMatrix = entity->GetParent()->transform.GetLocalToWorldMatrix() * CalculateLocalToParentMatrix();
		}
		else
		{
			localToWorldMatrix = CalculateLocalToParentMatrix();
		}
	}
	return localToWorldMatrix;
}

Mat4 Transform::GetWorldToLocalMatrix()
{
	if (isInverseDirty)
	{
		isInverseDirty = false;
		worldToLocalMatrix = GetLocalToWorldMatrix().Inverse();
	}
	return worldToLocalMatrix;
}

void Transform::SetDirty()
{
	if (isDirty) { return; }

	isDirty = true;
	isInverseDirty = true;
	for (Entity * e : entity->GetChildren())
	{
		e->transform.SetDirty();
	}
}

Json::Value Transform::ToJson() const
{
	Json::Value json;
	const auto & toJson = PType<Vec3>::GetInstance()->toJson;
	json["position"] = toJson(&position);
	json["rotation"] = toJson(&rotation);
	json["scale"] = toJson(&scale);
	return json;
}

void Transform::SetFromJson(const Json::Value & json)
{
	const auto & fromJson = PType<Vec3>::GetInstance()->fromJson;
	auto tempPos = (Vec3 *)fromJson(json["position"]);
	auto tempRot = (Vec3 *)fromJson(json["rotation"]);
	auto tempScale = (Vec3 *)fromJson(json["scale"]);
	position = *tempPos;
	rotation = *tempRot;
	scale = *tempScale;
	delete tempPos;
	delete tempRot;
	delete tempScale;
}
