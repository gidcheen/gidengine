#include "CameraComponent.h"

#include "GameHeader.h"


CameraComponent::CameraComponent(Entity * entity, const Json::Value & json) :
	Component(entity, json),
	fov(GetProperties(), 70, "fov"),
	height(GetProperties(), 10, "height"),
	aspectRatio(GetProperties(), 16.f / 9.f, "aspectRatio", "Aspect ration"),
	nearPlane(GetProperties(), 0.2f, "nearPlane", "The near plane"),
	farPlane(GetProperties(), 500.f, "farPlane", "The far plane"),
	isOrthographic(GetProperties(), false, "isOrthographic", "Orthographic rendering"),
	camera(Mat4(1, true), Mat4(1, true))
{
	SetReceivesRender(true);
}

void CameraComponent::Start()
{
	camera.SetProjectionMat(CalculatePRMat());
	camera.SetViewMat(GetEntity()->transform.GetWorldToLocalMatrix());
}

void CameraComponent::RenderUpdate()
{
	if (game.window->GetResChanged())
	{
		camera.SetProjectionMat(CalculatePRMat());
	}
	camera.SetViewMat(GetEntity()->transform.GetWorldToLocalMatrix());
	game.renderer3D->SubmitCamera(&camera);
}

Mat4 CameraComponent::CalculatePRMat() const
{
	if (isOrthographic)
	{
		float halfWidth = height * aspectRatio * 0.5f;
		float halfHeight = height * 0.5f;
		return Mat4::Orthographic(-halfWidth, halfWidth, -halfHeight, halfHeight, nearPlane, farPlane);
	}
	else
	{
		float aspect = (float)game.window->GetWidth() / (float)game.window->GetHeight();
		return Mat4::Perspective(fov, aspect, nearPlane, farPlane);
	}
}
