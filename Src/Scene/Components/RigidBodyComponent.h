#include "Scene/Component.h"

#include "GameHeader.h"
#include "Assets/Collision.h"


class RigidBodyComponent : public Component
{
NAME(RigidBodyComponent)
public:
	enum RigidBodyType
	{
		STATIC, KINEMATIC, DYNAMIC
	};

private:
	rp::RigidBody * rigidBody;

	PFloat mass;
	PInt bodyType;
	PFloat linearDamp;
	PFloat angularDamp;
	PBool isGravityEnabled;

	PCollisionRefVector collisions;
	vector<rp::ProxyShape *> collisionProxies;

public:
	RigidBodyComponent(Entity * entity, const Json::Value & json = Json::Value());

	//virtual void Awake() override;
	virtual void Destroy() override;
	virtual void Start() override;
	virtual void Stop() override;
	virtual void PrePhysicsUpdate() override;
	virtual void PostPhysicsUpdate() override;

	inline float GetMass() const { return mass; }
	void SetMass(float m);
	inline RigidBodyType GetType() const { return (RigidBodyType)(int)bodyType; }
	void SetRigidBodyType(RigidBodyType t);
	inline float GetLinearDamp() const { return linearDamp; }
	void SetLinearDamp(float damp);
	inline float GetAngularDamp() const { return angularDamp; }
	void SetAngularDamp(float damp);
	inline bool GetIsGravityEnabled() { return isGravityEnabled; }
	void SetIsGravityEnabled(bool g);

	vector<AssetRef<Collision>> GetCollision() const { return collisions; }
	vector<rp::ProxyShape *> GetCollisionProxies() const { return collisionProxies; }
	void AddCollision(AssetRef<Collision> cs);
	void RemoveCollision(int index);

	// todo: make global
	void SetVelocity(const Vec3 & vel) { rigidBody->setLinearVelocity(rp::Vector3(vel.x, vel.y, vel.z)); }
	void SetAngularVelocity(const Vec3 & vel) { rigidBody->setAngularVelocity(rp::Vector3(vel.x, vel.y, vel.z)); }

	void ApplyForce(const Vec3 & dir) { rigidBody->applyForceToCenterOfMass(rp::Vector3(dir.x, dir.y, dir.z)); }
	void ApplyForce(const Vec3 & dir, const Vec3 & pos)
	{
		rigidBody->applyForce(rp::Vector3(dir.x, dir.z, dir.y), rp::Vector3(pos.x, pos.y, pos.z));
	}
	void ApplyTorque(const Vec3 t) { rigidBody->applyTorque(rp::Vector3(t.x, t.y, t.z)); }

private:
	static Mat4 RpTransToMat4(const rp::Transform & trans);
	static rp::Transform Mat4ToRpTrans(const Mat4 & mat);
};

