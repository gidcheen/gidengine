#include "RigidBodyComponent.h"


RigidBodyComponent::RigidBodyComponent(Entity * entity, const Json::Value & json) :
	Component(entity, json),
	mass(GetProperties(), 1, "mass"),
	bodyType(GetProperties(), DYNAMIC, "bodyType"),
	linearDamp(GetProperties(), 0, "linearDamp"),
	angularDamp(GetProperties(), 0, "angularDamp"),
	collisions(GetProperties(), {}, "collisions"),
	isGravityEnabled(GetProperties(), true, "isGravityEnabled")
{
	SetReceivesPrePhysics(true);
	SetReceivesPostPhysics(true);

	rigidBody = game.physics->createRigidBody(rp::Transform::identity());

	rp::Transform t = Mat4ToRpTrans(GetEntity()->transform.GetLocalToWorldMatrix());
	rigidBody->setTransform(t);
	rigidBody->setUserData(this);
	rigidBody->setIsActive(GetIsActive());

	rp::Material m;
	m.setBounciness(0);
	m.setRollingResistance(0);
	rigidBody->setMaterial(m);

	SetMass(mass);
	SetRigidBodyType((RigidBodyType)(int)bodyType);
	SetLinearDamp(linearDamp);
	SetAngularDamp(angularDamp);

	vector<AssetRef<Collision>> tempCollisions = collisions;
	collisions->clear();

	// temp fix. should fall into lifecycle todo:
	for (const auto & tempCollision : tempCollisions)
	{
		AddCollision(tempCollision);
	}
}

void RigidBodyComponent::Destroy()
{
	game.physics->destroyRigidBody(rigidBody);
}

void RigidBodyComponent::Start()
{
	rigidBody->setIsActive(GetIsActive());
}

void RigidBodyComponent::Stop()
{
	rigidBody->setIsActive(GetIsActive());
}

void RigidBodyComponent::PrePhysicsUpdate()
{
	// todo: fix this with quaternion rotations.
	//rp::Transform t = Mat4ToRpTrans(GetEntity()->transform.GetLocalToWorldMatrix());
	//rigidBody->setTransform(t);
}

void RigidBodyComponent::PostPhysicsUpdate()
{
	Mat4 m = RpTransToMat4(rigidBody->getTransform());
	GetEntity()->transform.SetGlobalPosition(m.ToPosition());
	GetEntity()->transform.SetGlobalRotation(m.ToRotation());
	rigidBody->setIsActive(GetIsActive());
}

void RigidBodyComponent::SetMass(float m)
{
	mass = m;
	rigidBody->setMass(m);
}

void RigidBodyComponent::SetRigidBodyType(RigidBodyComponent::RigidBodyType t)
{
	bodyType = t;
	rigidBody->setType((rp::BodyType)t);
}

void RigidBodyComponent::SetLinearDamp(float damp)
{
	linearDamp = damp;
	rigidBody->setLinearDamping(damp);
}

void RigidBodyComponent::SetAngularDamp(float damp)
{
	angularDamp = damp;
	rigidBody->setLinearDamping(damp);
}

void RigidBodyComponent::SetIsGravityEnabled(bool g)
{
	rigidBody->enableGravity(g);
	isGravityEnabled = g;
}

void RigidBodyComponent::AddCollision(AssetRef<Collision> cs)
{
	const rp::Transform defaultTrans = Mat4ToRpTrans(cs->GetDefaultTransform());
	rp::ProxyShape * proxyShape = rigidBody->addCollisionShape(cs->GetShape(), defaultTrans, cs->GetDefaultWeight());
	ASSERT(proxyShape);
	proxyShape->setUserData(this);
	collisions->push_back(cs);
	collisionProxies.push_back(proxyShape);
	ASSERT(collisions->size() == collisionProxies.size());
}

void RigidBodyComponent::RemoveCollision(int index)
{
	if (index > collisionProxies.size()) { return; }
	rigidBody->removeCollisionShape(collisionProxies[index]);
	collisions->erase(collisions->begin() + index);
	collisionProxies.erase(collisionProxies.begin() + index);
}

Mat4 RigidBodyComponent::RpTransToMat4(const reactphysics3d::Transform & trans)
{
	Mat4 m;
	trans.getOpenGLMatrix(&m.elements[0]);
	return m;
}

rp::Transform RigidBodyComponent::Mat4ToRpTrans(const Mat4 & mat)
{
	rp::Transform t;
	t.setFromOpenGL((float *)&mat.elements[0]);
	return t;
}
