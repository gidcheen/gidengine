#include "FpsComponent.h"

#include "GameHeader.h"


FpsComponent::FpsComponent(Entity * entity, const Json::Value & json) :
	Component(entity, json),
	movementSpeed(GetProperties(), 1.5f, "movementSpeed"),
	rotationSpeed(GetProperties(), 1.5f, "rotationSpeed")
{
	SetReceivesUpdate(true);
}

void FpsComponent::Update(double dt)
{
	// movement
	Vec3 dir;
	if (game.window->IsKeyPressed(GLFW_KEY_W))
	{
		dir.z += -1;
	}
	if (game.window->IsKeyPressed(GLFW_KEY_S))
	{
		dir.z += 1;
	}
	if (game.window->IsKeyPressed(GLFW_KEY_A))
	{
		dir.x += -1;
	}
	if (game.window->IsKeyPressed(GLFW_KEY_D))
	{
		dir.x += 1;
	}
	if (game.window->IsKeyPressed(GLFW_KEY_Q))
	{
		dir.y += -1;
	}
	if (game.window->IsKeyPressed(GLFW_KEY_E))
	{
		dir.y += 1;
	}
	dir = GetEntity()->transform.GetForward() * dir.z +
		  GetEntity()->transform.GetRight() * dir.x +
		  GetEntity()->transform.GetUp() * dir.y;

	if (dir.Lenght() != 0) { dir.Normalize(); }

	float finalMovementSpeed = movementSpeed;
	if (game.window->IsKeyPressed(GLFW_KEY_LEFT_SHIFT))
	{
		finalMovementSpeed *= 4;
	}
	GetEntity()->transform.Translate(dir * (dt * finalMovementSpeed));

	// rotation
	Vec3 rot(-game.window->GetRelMousePos().y, -game.window->GetRelMousePos().x, 0);
	GetEntity()->transform.Rotate(Vec3(rot * (dt * rotationSpeed)));
}
