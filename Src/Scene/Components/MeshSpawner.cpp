#include "GameHeader.h"
#include "MeshSpawner.h"
#include "MeshComponent.h"


void MeshSpawner::Start()
{
}

void MeshSpawner::Update(double dt)
{
	if (game.window->IsKeyDown(GLFW_KEY_G))
	{
		Entity * spawnedEntity(new Entity(game.scene->GetRootEntity(), "spawned"));
		spawnedEntity->transform.SetPosition(
			GetEntity()->transform.GetGlobalPosition() + GetEntity()->transform.GetGlobalForward() * 0.5);
		spawnedEntity->transform.SetScale(Vec3(0.1, 0.1, 0.1));
		MeshComponent * c(new MeshComponent(spawnedEntity));
		c->SetMesh("Models/Fish.dae");
		c->SetMaterials({ "Materials/Scifi.material" });

		entities.push_back(spawnedEntity);
	}
	else if (game.window->IsKeyDown(GLFW_KEY_V) && entities.size() > 0)
	{
		game.scene->DeleteEntity(entities.back());
		entities.pop_back();
	}
}
