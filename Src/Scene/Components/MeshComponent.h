#pragma once

#include "../Component.h"
#include "Assets/Material.h"
#include "Assets/Mesh.h"
#include "Assets/AssetManagement/AssetRef.h"


class MeshComponent : public Component
{
NAME(MeshComponent);
private:
	PID meshId;
	AssetRef<Mesh> mesh;

	PIDVector materialIds;
	vector<AssetRef<Material>> materials;

public:
	MeshComponent(Entity * entity, const Json::Value & json = Json::Value());
	virtual ~MeshComponent();


	virtual void Awake() override;
	virtual void Start() override;
	virtual void RenderUpdate() override;

	inline AssetRef<Mesh> & GetMesh() { return mesh; };
	inline void SetMesh(const AssetRef<Mesh> & mesh)
	{
		this->mesh = mesh;
		meshId = mesh->GetId();
		materials.clear();
		materialIds->clear();
	}

	inline const vector<AssetRef<Material>> & GetMaterials() { return materials; };
	inline void SetMaterials(const vector<AssetRef<Material>> & materials)
	{
		if (mesh.IsEmpty())
		{
			LOG("Setting materials on empty mesh");
			return;
		}
		if (mesh->GetMaterialCount() != materials.size())
		{
			LOG("Setting wrong count of materials. Wanted: " + to_string(mesh->GetMaterialCount()) + " Given: " +
				to_string(materials.size()));
			return;
		}
		this->materials = materials;
		materialIds->resize(materials.size());
		for (int i = 0; i < materials.size(); i++)
		{
			materialIds[i] = materials[i]->GetId();
		}
	}
};
