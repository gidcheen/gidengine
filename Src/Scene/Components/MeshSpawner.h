#pragma once

#include "Scene/Component.h"


class MeshSpawner : public Component
{
NAME(MeshSpawner);
public:

	MeshSpawner(Entity * entity, const Json::Value & json = Json::Value()) :
		Component(entity, json) { SetReceivesUpdate(true); }

	virtual void Start() override;
	virtual void Update(double dt) override;

private:
	vector<Entity *> entities;
};
