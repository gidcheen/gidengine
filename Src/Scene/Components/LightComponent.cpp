#include "LightComponent.h"

#include "Graphics/LightStructs.h"
#include "GameHeader.h"
#include "Maths/Maths.h"
#include "Scene/Components/MeshComponent.h"


LightComponent::LightComponent(Entity * entity, const Json::Value & json) :
	Component(entity, json),
	lightType(GetProperties(), (int)LightType::point, "lightType"),
	color(GetProperties(), Vec4(1), "color"),
	strenght(GetProperties(), 1, "strenght"),
	innerCutOff(GetProperties(), 20, "innerCutOff"),
	outerCutOff(GetProperties(), 30, "outerCutOff"),
	shadow(GetProperties(), true, "shadow"),
	cameraMat("Materials/DepthOnly.material"),
	camera(Mat4(1, true), Mat4(1, true)),
	frameBuffer(nullptr)
{
	SetReceivesRender(true);
}

LightComponent::~LightComponent()
{
	delete frameBuffer;
}

void LightComponent::Awake()
{
	//frameBuffer = new GLFrameBuffer(1024, 1024, 1, false, true);
	//camera.SetFrameBuffer(frameBuffer);
	//camera.SetProjectionMat(Mat4::Orthographic(-40, 40, -40, 40, -40, 40));
}

void LightComponent::Start()
{
#ifdef EDITOR
	Entity * e = new Entity(this->GetEntity());
	e->transform.SetScale(Vec3(0.5, 0.5, 0.5));

	MeshComponent * mc = new MeshComponent(e);
	mc->SetMesh("Models/Sphere.dae");
	mc->SetMaterials({ "Materials/Unlit.material" });
#endif
}

void LightComponent::RenderUpdate()
{
	//camera.SetViewMat(GetEntity()->transform.GetLocalToWorldMatrix());
	//if (camera.GetFrameBuffer()) { camera.GetFrameBuffer()->Clear(); }
	//game.renderer3D->Render(&camera, cameraMat.GetAsset(), true);

	switch ((int)lightType)
	{
	case LightType::ambient:
	{
		game.renderer3D->SubmitAmbientLight({ color * strenght });
		break;
	}
	case LightType::directional:
	{
		game.renderer3D->SubmitDirectionalLight(
			{
				color * strenght,
				GetEntity()->transform.GetGlobalForward(),
				nullptr //camera.GetFrameBuffer()->GetDepthTexture()
			});
		break;
	}
	case LightType::point:
	{
		game.renderer3D->SubmitPointLight(
			{
				color * strenght,
				GetEntity()->transform.GetGlobalPosition(),
				4.f,
				nullptr //camera.GetFrameBuffer()->GetDepthTexture()
			});
		break;
	}
	case LightType::spot:
	{
		game.renderer3D->SubmitSpotLight(
			{
				color * strenght,
				GetEntity()->transform.GetGlobalPosition(),
				GetEntity()->transform.GetGlobalForward(),
				Math::ToRadians(innerCutOff),
				Math::ToRadians(outerCutOff),
				30.f,
				nullptr //camera.GetFrameBuffer()->GetDepthTexture()
			});
		break;
	}
	default:
		ASSERT(false);
		break;
	}
}
