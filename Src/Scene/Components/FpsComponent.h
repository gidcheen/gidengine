#include "Scene/Component.h"


class FpsComponent : public Component
{
public:
	NAME(FpsComponent);

	PFloat movementSpeed;
	PFloat rotationSpeed;

	FpsComponent(Entity * entity, const Json::Value & json = Json::Value());

	virtual void Update(double dt) override;
};