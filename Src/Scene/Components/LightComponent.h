#pragma once

#include "Assets/Mesh.h"
#include "Assets/AssetManagement/AssetRef.h"
#include "Scene/Component.h"
#include "Graphics/GLCamera.h"
#include "Graphics/Buffers/GLFrameBuffer.h"


class LightComponent : public Component
{
public:
NAME(LightComponent);

	PInt lightType;
	PVec4 color;
	PFloat strenght;
	PFloat innerCutOff;
	PFloat outerCutOff;
	PBool shadow;

private:
	AssetRef<Mesh> sphereMesh;
	AssetRef<Material> cameraMat;
	GLCamera camera;
	GLFrameBuffer * frameBuffer;

public:
	LightComponent(Entity * entity, const Json::Value & json = Json::Value());
	virtual ~LightComponent();

	virtual void Awake() override;
	virtual void Start() override;
	virtual void RenderUpdate() override;
};
