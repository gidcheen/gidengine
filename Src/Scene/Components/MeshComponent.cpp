#include "MeshComponent.h"
#include "GameHeader.h"


MeshComponent::MeshComponent(Entity * entity, const Json::Value & json) :
	Component(entity, json),
	meshId(GetProperties(), 0, "meshId"),
	materialIds(GetProperties(), vector<ID>(), "materialIds")
{
	SetReceivesRender(true);
}

MeshComponent::~MeshComponent()
{
}

void MeshComponent::Awake()
{
	mesh.SetAsset(meshId);
}

void MeshComponent::Start()
{
}

void MeshComponent::RenderUpdate()
{
//#ifdef DEBUG
	if (mesh.IsEmpty())
	{
		LOG("Empty mesh component in entity: " + GetEntity()->name);
		return;
	}
	if (materials.size() != mesh->GetMaterialCount())
	{
		LOG("Mesh with no materials");
		return;
	}
//#endif
	vector<Material *> materialPtrs(materials.size());
	for (int i = 0; i < materials.size(); i++)
	{
		materialPtrs[i] = materials[i].GetAsset();
		ASSERT(materialPtrs[i]);
	}

	game.renderer3D->SubmitMesh(mesh.GetAsset(), materialPtrs, GetEntity()->transform.GetLocalToWorldMatrix());
}
