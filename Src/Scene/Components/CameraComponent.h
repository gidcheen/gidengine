#pragma once

#include "Scene/Component.h"
#include "Graphics/Renders/Renderer3D.h"
#include "Graphics/GLCamera.h"


class CameraComponent : public Component
{
public:
NAME(CameraComponent);

	// perspective
	PFloat fov;

	// otrtographic
	PFloat height;

	PFloat aspectRatio;
	PFloat nearPlane;
	PFloat farPlane;

	PBool isOrthographic;

private:
	GLCamera camera;

public:
	CameraComponent(Entity * entity, const Json::Value & json = Json::Value());
	virtual ~CameraComponent() {};

	virtual void Start() override;
	virtual void RenderUpdate() override;

	Mat4 CalculatePRMat() const;
};

