#include "Entity.h"

#include "GameHeader.h"
#include "Component.h"


Entity::Entity(Entity * parent) :
	Entity(parent, string("Entity"))
{
}

Entity::Entity(Entity * parent, const string & name) :
	name(name),
	transform(this),
	parent(nullptr)
{
	if (parent)
	{
		SetParent(parent);
	}
	else
	{
		game.scene->SetRootEntity(this);
	}
	game.scene->AddEntity(this);
	
#ifdef DEBUG
	Vec3 v;
	int i(4);
	int j = 4;
#endif
}

Entity::Entity(const Json::Value & json, Entity * parent) :
	Entity(parent, json["name"].asString())
{
	transform.SetFromJson(json["transform"]);
	for (const auto & compJson : json["components"])
	{
		// entity handels destruction
		CompData d = { this, compJson };
		Component * component = game.scene->GetComponentFactory().Create(compJson["name"].asString(), d);
	}
	for (const auto & childJson: json["children"])
	{
		// scene handels destruction
		new Entity(childJson, this);
	}
}

void Entity::SetParent(Entity * parent)
{
	ASSERT(parent);
	if (this->parent) { this->parent->RemoveChild(this); };
	this->parent = parent;
	parent->children.push_back(this);
}

Entity::~Entity()
{
}

Entity * Entity::GetChild(int index)
{
	ASSERT(index < children.size());
	return children[index];
}

void Entity::AddComponent(Component * component)
{
	components.push_back(component);
}

Json::Value Entity::ToJson() const
{
	Json::Value json;
	json["name"] = name.ToString();
	json["transform"] = transform.ToJson();

	Json::Value componentsJson;
	for (const auto & component : components)
	{
		componentsJson.append(component->ToJson());
	}
	json["components"] = componentsJson;

	Json::Value childrenJson;
	for (const auto & c : children)
	{
		childrenJson.append(c->ToJson());
	}
	json["children"] = childrenJson;

	return json;
}

void Entity::RemoveChild(Entity * child)
{
	children.erase(remove(children.begin(), children.end(), child), children.end());
}
