#pragma once

#include <vector>
#include <string>
#include <json/json.h>

#include "Utils/DynamicFactory.h"
#include "Utils/Hash.h"
#include "Utils/Assert.h"


class Entity;

class Component;

using namespace std;

struct CompData
{
	Entity * e;
	Json::Value j;
};

class Scene
{
public:
	friend class Entity;

private:
	Entity * rootEntity;
	vector<Entity *> entities;
	vector<Entity *> entitiesToDestroy;

	// Component lifecycle
	vector<Component *> toAwake;
	vector<Component *> toDestroy;
	vector<Component *> toStart;
	vector<Component *> toStop;
	vector<Component *> toUpdate;
	vector<Component *> toPrePhysics;
	vector<Component *> toPostPhysics;
	vector<Component *> toRender;

	DynamicFactory<Component, CompData> componentFactory;

public:
	Scene();
	~Scene();

	Entity * GetRootEntity() { return rootEntity; }

	void Awake();
	void Destroy();
	void Start();
	void Stop();
	void Update(double dt);
	void PrePhysics();
	void PostPhysics();
	void Render();

	void AddToAwake(Component * c) { toAwake.push_back(c); }
	void AddToDestroy(Component * c) { toDestroy.push_back(c); }
	void AddToStart(Component * c) { toStart.push_back(c); }
	void AddToStop(Component * c) { toStop.push_back(c); }

	void AddToUpdate(Component * c) { toUpdate.push_back(c); }
	void RemoveFromUpdate(Component * c);
	void AddToPrePhysics(Component * c) { toPrePhysics.push_back(c); }
	void RemoveFromPrePhysics(Component * c);
	void AddToPostPhysics(Component * c) { toPostPhysics.push_back(c); }
	void RemoveFromPostPhysics(Component * c);
	void AddToRender(Component * c) { toRender.push_back(c); }
	void RemoveFromRender(Component * c);

	void SaveScene(const string & path);
	void SaveScene(const Entity * root, const string & path);

	bool LoadScene(const string & path);
	bool LoadScene(Entity * root, const string & path);

	void DeleteEntity(Entity * entity);

	inline DynamicFactory<Component, CompData> & GetComponentFactory() { return componentFactory; }

private:
	void AddEntity(Entity * entity) { entities.push_back(entity); };
	void DestroyEntity(Entity * entity);

	void DestroyComponent(Component * c);

	inline void SetRootEntity(Entity * entity)
	{
		ASSERT(!rootEntity);
		rootEntity = entity;
	}

	inline bool CanUpdate(Component * c);
};
