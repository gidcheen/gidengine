#pragma once

#include <vector>
#include <string>

#include "Transform.h"
#include "Utils/Name.h"


using namespace std;

class Component;

class Entity
{
public:
	friend class Scene;
public:
	Name name;
	Transform transform;

private:
	Entity * parent;
	vector<Entity *> children;

	vector<Component *> components;

public:
	Entity(Entity * parent);
	Entity(Entity * parent, const string & name);
	Entity(const Json::Value & json, Entity * parent);
	~Entity();

	auto GetParent() { return parent; }
	void SetParent(Entity * parent);

	inline vector<Entity *> GetChildren() { return children; };

	inline vector<Component *> GetComponents() { return components; }

	Entity * GetChild(int index);
	Entity * GetChild(const Name & name);

	void AddComponent(Component * component);

	Json::Value ToJson() const;

	inline bool IsRoot() const { return (bool)!parent; }

private:
	void RemoveChild(Entity * child);

};
