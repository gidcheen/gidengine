#pragma once

#include "Entity.h"
#include "PropertySystem/PropertySystem.h"
#include "Utils/Named.h"


class Component : public Named
{
private:
	Entity * entity;
	bool isActive;

	bool receivesUpdate;
	bool receivesPrePhysics;
	bool receivesPostPhysics;
	bool receivesRender;

	PropertyHolder properties;

	bool markedForDelete;

public:
	Component(Entity * entity, const Json::Value & json);
	virtual ~Component() {}

	inline Entity * GetEntity() { return entity; }

	inline bool GetIsActive() { return isActive; }
	inline void SetIsActive(bool ia);

	inline PropertyHolder & GetProperties() { return properties; }

	inline bool GetReceivesUpdate() { return receivesUpdate; }
	void SetReceivesUpdate(bool r);

	inline bool GetReceivesPrePhysics() { return receivesPrePhysics; }
	void SetReceivesPrePhysics(bool r);

	inline bool GetReceivesPostPhysics() { return receivesPostPhysics; }
	void SetReceivesPostPhysics(bool r);

	inline bool GetReceivesRender() { return receivesRender; }
	void SetReceivesRender(bool r);


	virtual void Awake() {}
	virtual void Destroy() {}
	virtual void Start() {}
	virtual void Stop() {}
	virtual void Update(double dt) {}
	virtual void PrePhysicsUpdate() {}
	virtual void PostPhysicsUpdate() {}
	virtual void RenderUpdate() {}


	Json::Value ToJson() const;
	void SetFromJson(const Json::Value & json);

	inline bool GetMarkedForDelete() { return markedForDelete; }
	inline void SetMarkedForDelete() { markedForDelete = true; }
};
