#pragma once

template<typename T, unsigned X, unsigned Y>
struct Mat;

template<unsigned X, unsigned Y>
using Matf = Mat<float, X, Y>;
template<unsigned X, unsigned Y>
using Matd = Mat<double, X, Y>;

using Mat2 = Mat<float, 2, 2>;
using Mat2f = Mat<float, 2, 2>;
using Mat2d = Mat<double, 2, 2>;

using Mat3 = Mat<float, 3, 3>;
using Mat3f = Mat<float, 3, 3>;
using Mat3d = Mat<double, 3, 3>;

using Mat4 = Mat<float, 4, 4>;
using Mat4f = Mat<float, 4, 4>;
using Mat4d = Mat<double, 4, 4>;


template<typename T, unsigned S>
using Vec = Mat<T, S, 1>;

using Vec2 = Vec<float, 2>;
using Vec2f = Vec<float, 2>;
using Vec2d = Vec<double, 2>;

using Vec3 = Vec<float, 3>;
using Vec3f = Vec<float, 3>;
using Vec3d = Vec<double, 3>;

using Vec4 = Vec<float, 4>;
using Vec4f = Vec<float, 4>;
using Vec4d = Vec<double, 4>;
