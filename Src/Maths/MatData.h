#pragma once

#include <array>
#include <type_traits>


using namespace std;


// default mat
template<typename T, unsigned X, unsigned Y, typename Enable = void>
struct MatData
{
	union
	{
		T rawElements[X * Y];
		T rawColumns[Y][X];
		array<T, X * Y> elements;
		array<array<T, X>, Y> columns;
	};
};

// hack for CLion highlighting // enable_if_t<> with false doesnt wortk
template<typename T, unsigned X, unsigned Y>
struct MatData<T, X, Y, enable_if_t<X == -1 && false>>
{
	union
	{
		T rawElements[X * Y];
		T rawColumns[Y][X];
		array<T, X * Y> elements;
		array<array<T, X>, Y> columns;
	};
};

// zero size matrix
template<typename T, unsigned X, unsigned Y>
struct MatData<T, X, Y, enable_if_t<X == 0 || Y == 0>>
{
};


// vector specializations
template<typename T, unsigned X, unsigned Y>
struct MatData<T, X, Y, enable_if_t<X == 1 && Y == 1>>
{
	union
	{
		T rawElements[X * Y];
		T rawColumns[Y][X];
		array<T, X * Y> elements;
		array<array<T, X>, Y> columns;
		struct
		{
			T x;
		};
	};
};

template<typename T, unsigned X, unsigned Y>
struct MatData<T, X, Y, enable_if_t<X == 2 && Y == 1>>
{
	union
	{
		T rawElements[X * Y];
		T rawColumns[Y][X];
		array<T, X * Y> elements;
		array<array<T, X>, Y> columns;
		struct
		{
			T x;
			T y;
		};
	};
};

template<typename T, unsigned X, unsigned Y>
struct MatData<T, X, Y, enable_if_t<X == 3 && Y == 1>>
{
	union
	{
		T rawElements[X * Y];
		T rawColumns[Y][X];
		array<T, X * Y> elements;
		array<array<T, X>, Y> columns;
		struct
		{
			T x;
			T y;
			T z;
		};
		struct
		{
			T r;
			T g;
			T b;
		};
	};
};

template<typename T, unsigned X, unsigned Y>
struct MatData<T, X, Y, enable_if_t<X == 4 && Y == 1>>
{
	union
	{
		T rawElements[X * Y];
		T rawColumns[Y][X];
		array<T, X * Y> elements;
		array<array<T, X>, Y> columns;
		struct
		{
			T x;
			T y;
			T z;
			T w;
		};
		struct
		{
			T r;
			T g;
			T b;
			T a;
		};
	};
};
