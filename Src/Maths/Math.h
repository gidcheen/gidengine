#pragma once

#include <cmath>


class Math
{
public:
	static float ToRadians(float degrees) { return degrees * (float(M_PI) / 180.f); }
	static float ToEuler(float radians) { return (radians * 180.f) / float(M_PI); }
	static double ToRadians(double degrees) { return degrees * (M_PI / 180.0); }
	static double ToEuler(double radians) { return (radians * 180.0) / M_PI; }
};
