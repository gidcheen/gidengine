#pragma once

#include "Mat.h"

#include "Utils/Assert.h"


template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::Set(const T * values)
{
	ASSERT(values);
	for (int i = 0; i < X * Y; ++i)
	{
		this->elements[i] = values[i];
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
template<unsigned X2, unsigned Y2>
Mat<T, X, Y> & Mat<T, X, Y>::Set(const Mat<T, X2, Y2> & other)
{
	for (int i = 0; i < (Y < Y2 ? Y : Y2); ++i)
	{
		for (int j = 0; j < (X < X2 ? X : X2); ++j)
		{
			this->columns[i][j] = other.columns[i][j];
		}
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
template<unsigned M2Y>
Mat<T, X, M2Y> Mat<T, X, Y>::operator*(const Mat<T, Y, M2Y> & other) const
{
	Mat<T, X, M2Y> ret;
	for (int row = 0; row < X; ++row)
	{
		for (int col = 0; col < M2Y; ++col)
		{
			for (int el = 0; el < Y; ++el)
			{
				ret[col][row] += this->columns[el][row] * other[col][el];
			}
		}
	}
	return ret;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::operator+=(const Mat<T, X, Y> & other)
{
	for (int i = 0; i < X * Y; ++i)
	{
		this->elements[i] += other.elements[i];
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::operator-=(const Mat<T, X, Y> & other)
{
	for (int i = 0; i < X * Y; ++i)
	{
		this->elements[i] -= other.elements[i];
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::operator*=(const T & other)
{
	for (int i = 0; i < X * Y; ++i)
	{
		this->elements[i] *= other;
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
bool Mat<T, X, Y>::operator==(const Mat<T, X, Y> & other) const
{
	for (int i = 0; i < Y * Y; ++i)
	{
		if (this->elements[i] != other.elements[i]) { return false; }
	}
	return true;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::Fill(const T & v)
{
	this->elements.fill(v);
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::SetDiagonale(const T & value)
{
	for (int i = 0; i < X * Y; i += X + 1)
	{
		this->elements[i] = value;
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, Y, X> Mat<T, X, Y>::Transposed() const
{
	Mat<T, Y, X> ret;
	for (int i = 0; i < Y; ++i)
	{
		for (int j = 0; j < X; ++j)
		{
			ret.columns[j][i] = this->columns[i][j];
		}
	}
	return ret;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X - 1, Y - 1> Mat<T, X, Y>::GetMinor(unsigned indexX, unsigned indexY)
{
	Mat<T, X - 1, Y - 1> ret;
	int skipY = 0;
	for (int i = 0; i < Y - 1; ++i)
	{
		skipY += (i == indexY ? 1 : 0);
		int skipX = 0;
		for (int j = 0; j < X - 1; ++j)
		{
			skipX += (j == indexX ? 1 : 0);
			ret[i][j] = this->columns[i + skipY][j + skipX];
		}
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
string Mat<T, X, Y>::ToString() const
{
	string ret;
	for (int i = 0; i < X; ++i)
	{
		for (int j = 0; j < Y; ++j)
		{
			string s(to_string(this->columns[j][i]).substr(0, 8));
			ret += "[" + s + "]";
		}
		ret += '\n';
	}
	ret.pop_back();
	return ret;
}

template<typename T, unsigned X, unsigned Y>
template<unsigned, unsigned, typename>
Mat<T, X, Y> & Mat<T, X, Y>::Transpose()
{
	for (int i = 0; i < Y; ++i)
	{
		for (int j = i + 1; j < X; ++j)
		{
			T temp = this->columns[i][j];
			this->columns[i][j] = this->columns[j][i];
			this->columns[j][i] = temp;
		}
	}
	return *this;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned, unsigned, typename>
T Mat<T, X, Y>::GetDeterminante(int)
{
	T ret = T();
	for (unsigned i = 0; i < X; ++i)
	{
		float sign = i % 2 == 0 ? 1.f : -1.f;
		ret += this->columns[i][0] * this->GetMinor(0, i).GetDeterminante() * sign;
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned, unsigned, typename>
Mat<T, X, X> & Mat<T, X, Y>::Invert()
{
	Mat<T, X, X> inv;
	T det = T();//= this->GetDeterminante();

	for (unsigned i = 0; i < X; ++i)
	{
		for (unsigned j = 0; j < X; ++j)
		{
			float sign = i % 2 == j % 2 ? 1.f : -1.f;
			inv[i][j] = this->GetMinor(j, i).GetDeterminante() * sign;
		}
	}
	inv.Transpose();
	for (int i = 0; i < X; ++i)
	{
		det += this->columns[0][i] * inv.columns[i][0];
	}
	ASSERT(det != 0); //ASSERT(det >= 1e-7 || det <= -1e-7);
	*this = inv * (1 / det);
	return *this;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned X2, unsigned Y2, typename>
Mat<T, X, X> & Mat<T, X, Y>::Invert(int)
{
	T * m = this->rawElements;
	T inv[16];
	T det;

	inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6] * m[15] + m[9] * m[7] * m[14] + m[13] * m[6] * m[11] -
			 m[13] * m[7] * m[10];
	inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15] - m[8] * m[7] * m[14] -
			 m[12] * m[6] * m[11] + m[12] * m[7] * m[10];
	inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5] * m[15] + m[8] * m[7] * m[13] + m[12] * m[5] * m[11] -
			 m[12] * m[7] * m[9];
	inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5] * m[14] - m[8] * m[6] * m[13] - m[12] * m[5] * m[10] +
			  m[12] * m[6] * m[9];
	inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2] * m[15] - m[9] * m[3] * m[14] -
			 m[13] * m[2] * m[11] + m[13] * m[3] * m[10];
	inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2] * m[15] + m[8] * m[3] * m[14] + m[12] * m[2] * m[11] -
			 m[12] * m[3] * m[10];
	inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1] * m[15] - m[8] * m[3] * m[13] - m[12] * m[1] * m[11] +
			 m[12] * m[3] * m[9];
	inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1] * m[14] + m[8] * m[2] * m[13] + m[12] * m[1] * m[10] -
			  m[12] * m[2] * m[9];
	inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2] * m[15] + m[5] * m[3] * m[14] + m[13] * m[2] * m[7] -
			 m[13] * m[3] * m[6];
	inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2] * m[15] - m[4] * m[3] * m[14] - m[12] * m[2] * m[7] +
			 m[12] * m[3] * m[6];
	inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1] * m[15] + m[4] * m[3] * m[13] + m[12] * m[1] * m[7] -
			  m[12] * m[3] * m[5];
	inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1] * m[14] - m[4] * m[2] * m[13] - m[12] * m[1] * m[6] +
			  m[12] * m[2] * m[5];
	inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11] - m[5] * m[3] * m[10] - m[9] * m[2] * m[7] +
			 m[9] * m[3] * m[6];
	inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11] + m[4] * m[3] * m[10] + m[8] * m[2] * m[7] -
			 m[8] * m[3] * m[6];
	inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11] - m[4] * m[3] * m[9] - m[8] * m[1] * m[7] +
			  m[8] * m[3] * m[5];
	inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10] + m[4] * m[2] * m[9] + m[8] * m[1] * m[6] -
			  m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
	ASSERT(det != 0);
	det = 1.0f / det;
	for (int i = 0; i < 16; i++) { m[i] = inv[i] * det; }
	return *this;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned, unsigned, typename>
Mat<T, 3, 1> Mat<T, X, Y>::ToRotation() const
{
	Mat<T, 3, 1> ret;
	const auto & m = *this;

	float e02 = m[0][2];
	if (e02 < 1 - 1e-6f && e02 > -1 + 1e-6f)
	{
		ret.y = -asinf(e02);
		float oneDivCosY = 1 / cosf(ret.y);
		ret.x = atan2f(m[1][2] * oneDivCosY, m[2][2] * oneDivCosY);
		ret.z = atan2f(m[0][1] * oneDivCosY, m[0][0] * oneDivCosY);
	}
	else
	{
		if (e02 > 0)
		{
			ret.y = M_PI / 2.0;
			ret.x = atan2(-m[1][0], -m[2][0]);
		}
		else
		{
			ret.y = M_PI / 2.0;
			ret.x = atan2(m[1][0], m[2][0]);
		}
	}
	ret.x = Math::ToEuler(ret.x);
	ret.y = Math::ToEuler(ret.y);
	ret.z = Math::ToEuler(ret.z);
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::Orthographic(float left, float right, float bottom, float top, float near, float far)
{
	Mat<T, X, Y> ret;

	ret[0][0] = 2.0f / (right - left);
	ret[1][1] = 2.0f / (top - bottom);
	ret[2][2] = -2.0f / (far - near);

	ret[3][0] = -(right + left) / (right - left);
	ret[3][1] = -(top + bottom) / (top - bottom);
	ret[3][2] = -(far + near) / (far - near);
	ret[3][3] = 1;

	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::Perspective(float fov, float apsect, float near, float far)
{
	Mat<T, X, Y> ret;

	float q = 1.0f / tanf(Math::ToRadians(0.5f * fov));
	float a = q / apsect;
	float b = -(far + near) / (far - near);
	float c = -(2.0f * far * near) / (far - near);

	ret[0][0] = a;
	ret[1][1] = q;
	ret[2][2] = b;
	ret[2][3] = -1.0f;
	ret[3][2] = c;

	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::Translation(const Mat<T, 3, 1> & translation)
{
	Mat<T, X, Y> ret(1, true);
	ret[3][0] = translation.x;
	ret[3][1] = translation.y;
	ret[3][2] = translation.z;
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::Rotation(T angle, const Mat<T, 3, 1> & axis)
{
	Mat<T, X, Y> ret(1, true);

	T r = Math::ToRadians(angle);
	T c = cos(r);
	T s = sin(r);
	T omc = 1.0f - c;

	T x = axis.x;
	T y = axis.y;
	T z = axis.z;

	ret[0][0] = x * x * omc + c;
	ret[0][1] = x * y * omc + z * s;
	ret[0][2] = x * z * omc - y * s;

	ret[1][0] = x * y * omc - z * s;
	ret[1][1] = y * y * omc + c;
	ret[1][2] = y * z * omc + x * s;

	ret[2][0] = x * z * omc + y * s;
	ret[2][1] = y * z * omc - x * s;
	ret[2][2] = z * z * omc + c;

	return ret;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> Mat<T, X, Y>::Rotation(const Mat<T, 3, 1> & rotation)
{

	return Mat4::Rotation(rotation.z, Mat<T, 3, 1>(0, 0, 1)) *
		   Mat4::Rotation(rotation.y, Mat<T, 3, 1>(0, 1, 0)) *
		   Mat4::Rotation(rotation.x, Mat<T, 3, 1>(1, 0, 0));
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> Mat<T, X, Y>::Scale(const Mat<T, 3, 1> & scale)
{
	Mat<T, X, Y> ret(1, true);
	ret[0][0] = scale.x;
	ret[1][1] = scale.y;
	ret[2][2] = scale.z;
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::Transform(const Mat<T, 3, 1> & position, const Mat<T, 3, 1> & rotation, const Mat<T, 3, 1> & scale)
{
	return Mat4::Translation(position) * Mat4::Rotation(rotation) * Mat4::Scale(scale);
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned, unsigned, typename>
T Mat<T, X, Y>::Lenght()
{
	T pows = T();
	for (int i = 0; i < X; i++)
	{
		pows += pow(this->elements[i], 2);
	}
	return sqrt(pows);
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned, unsigned, typename>
Mat<T, X, Y> Mat<T, X, Y>::Cross(const Mat<T, X, Y> & other)
{
	Mat<T, X, Y> ret;
	ret.x = this->y * other.z - this->z * other.y;
	ret.y = this->z * other.x - this->x * other.z;
	ret.z = this->x * other.y - this->y * other.x;
	return ret;
}
