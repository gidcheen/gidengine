#include "Renderer3D.h"

#include <algorithm>

#include "Graphics/GLCamera.h"
#include "Graphics/Window.h"


Renderer3D::Renderer3D() :
	multiSampleFbo(game.window->GetWidth(), game.window->GetHeight(), game.graphicsSettings.msaa),
	renderFbo(game.window->GetWidth(), game.window->GetHeight()),
	frameMesh("Models/FrameBufferMesh.dae"),
	finalMat("Materials/FrameBufferMat.material")
{
}

void Renderer3D::SubmitMesh(Mesh * mesh, const vector<Material *> & materials, const Mat4 & transform)
{
	ASSERT(mesh);
	for (int i = 0; i < materials.size(); i++)
	{
		Renderable r(mesh, i, materials[i], transform);
		renderQueue.push_back(r);
	}
}

void Renderer3D::Render(const GLCamera * camera, const Material * overrideMaterial, bool depthOnly)
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	if (!depthOnly)
	{
		sort(renderQueue.begin(), renderQueue.end(), bind(SortRenderQueue, placeholders::_1, placeholders::_2, camera));
	}

	const Shader * shader = nullptr;
	const Material * material = nullptr;
	const GLFrameBuffer * frameBuffer = camera->GetFrameBuffer() ? camera->GetFrameBuffer() : &multiSampleFbo;
	ASSERT(frameBuffer->GetTexture() || frameBuffer->GetDepthTexture());

	// set the viewport
	const GLTexture * tempTex = frameBuffer->GetTexture() ? frameBuffer->GetTexture() : frameBuffer->GetDepthTexture();
	glViewport(0, 0, tempTex->GetWidth(), tempTex->GetHeight());


	int rSize = (int)renderQueue.size();
	for (int i = 0; i < rSize; i++)
	{
		GLenum additiveBlend = GL_ONE;
		Renderable & r = renderQueue[i];

		// shader and material setup
		if (shader != r.material->GetShader())
		{
			if (shader) { shader->Disable(); }
			shader = r.material->GetShader();

			shader->Enable();
			shader->SetUniformMat4("projectionMat", camera->GetProjectionMat());
			shader->SetUniformMat4("viewMat", camera->GetViewMat());
		}
		shader->SetUniformMat4("modelMat", r.transform);

		if (material != r.material)
		{
			material = overrideMaterial ? overrideMaterial : r.material;
			material->SetShaderProperties();
		}

		if (material->GetTransparency() == Material::translucent)
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			additiveBlend = GL_SRC_ALPHA;
		}
		else
		{
			glDisable(GL_BLEND);
		}


		vector<AmbientLight> als = ambientLights;
		vector<DirectionalLight> dls = directionalLights;
		vector<PointLight> pls = pointLights;
		vector<SpotLight> sls = spotLights;
		for (int j = (int)pls.size() - 1; j >= 0; j--)
		{
			float dist = (pls[j].position - Vec3(r.transform.rawColumns[3])).Lenght();
			if (dist > pls[j].range)
			{
				pls.erase(pls.begin() + j);
			}
		}
		for (int j = (int)sls.size() - 1; j >= 0; j--)
		{
			if ((sls[j].position - Vec3(r.transform.rawColumns[3])).Lenght() > sls[j].range)
			{
				sls.erase(sls.begin() + j);
			}
		}


		glDepthFunc(GL_LESS);
		do
		{
			if (als.empty())
			{
				shader->SetUniformV4(alUniformName + "color", Vec4(0.f));
			}
			else
			{
				shader->SetUniformV4(alUniformName + "color", als.back().color);
				als.pop_back();
			}
			if (dls.empty())
			{
				shader->SetUniformV4(dlUniformName + "color", Vec4(0.f));
				shader->SetUniformI(dlUniformName + "shadow", false);
			}
			else
			{
				const auto & dl = dls.back();
				bool useShadow = dl.shadowDepth != nullptr;
				shader->SetUniformV4(dlUniformName + "color", dl.color);
				shader->SetUniformV3(dlUniformName + "direction", Vec3(camera->GetViewMat() * Vec4(dl.direction, 0)));
				shader->SetUniformI(dlUniformName + "useShadow", useShadow);
				if (useShadow) { shader->SetTexture(dlUniformName + "shadowDepth", dl.shadowDepth, 29); }
				dls.pop_back();
			}
			if (pls.empty())
			{
				shader->SetUniformV4(plUniformName + "color", Vec4(0.f));
				shader->SetUniformI(plUniformName + "shadow", false);
			}
			else
			{
				const auto pl = pls.back();
				bool useShadow = pl.shadowDepth != nullptr;
				shader->SetUniformV4(plUniformName + "color", pl.color);
				shader->SetUniformV3(plUniformName + "position", Vec3(camera->GetViewMat() * Vec4(pl.position, 1)));
				shader->SetUniformI(plUniformName + "useShadow", useShadow);
				if (useShadow) { shader->SetTexture(plUniformName + "shadowDepth", pl.shadowDepth, 30); }
				pls.pop_back();
			}
			if (sls.empty())
			{
				shader->SetUniformV4(slUniformName + "color", Vec4(0.f));
				shader->SetUniformI(slUniformName + "shadow", false);
			}
			else
			{
				const auto & sl = sls.back();
				bool useShadow = sl.shadowDepth != nullptr;
				shader->SetUniformV4(slUniformName + "color", sl.color);
				shader->SetUniformV3(slUniformName + "position", Vec3(camera->GetViewMat() * Vec4(sl.position, 1)));
				shader->SetUniformV3(slUniformName + "direction", Vec3(camera->GetViewMat() * Vec4(sl.direction, 0)));
				shader->SetUniformF(slUniformName + "innerCutOff", sl.innerCutOff);
				shader->SetUniformF(slUniformName + "outerCutOff", sl.outerCutOff);
				shader->SetUniformI(slUniformName + "useShadow", useShadow);
				if (useShadow) { shader->SetTexture(slUniformName + "shadowDepth", sl.shadowDepth, 31); }
				sls.pop_back();
			}


			// actual drawing to the framebuffer
			r.mesh->BindBuffers(r.materialIndex);
			{
				frameBuffer->Bind();
				{
					const GLuint indexCount = r.mesh->indexBuffers[r.materialIndex]->GetCount();
					glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, nullptr);
				}
				frameBuffer->Unbind();
			}
			r.mesh->UnbindBuffers(r.materialIndex);
			numDrawCalls++;

			glEnable(GL_BLEND);
			glBlendFunc(additiveBlend, GL_ONE);
			glDepthFunc(GL_EQUAL);
		} while (!als.empty() || !dls.empty() || !pls.empty() || !sls.empty());
	}
	if (shader) { shader->Disable(); }

	// todo: redo
	for (auto & ps : camera->postEffects)
	{
		// needs a single sample buffer
		//RenderFrameBuffer(frameBuffer, ps.GetAsset());
	}

	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
}

void Renderer3D::Flush()
{
	if (game.window->GetResChanged())
	{
		multiSampleFbo.Resize(game.window->GetWidth(), game.window->GetHeight());
		renderFbo.Resize(game.window->GetWidth(), game.window->GetHeight());
	}

	multiSampleFbo.Clear();
	renderFbo.Clear();

	for (const GLCamera * c : mainCameras)
	{
		Render(c);
	}

	// everything is rendered now clear
	mainCameras.clear();
	renderQueue.clear();
	ambientLights.clear();
	directionalLights.clear();
	pointLights.clear();
	spotLights.clear();

	//LOG(to_string(numDrawCalls));
	numDrawCalls = 0;


	// final srgb copy
	glEnable(GL_FRAMEBUFFER_SRGB);
	multiSampleFbo.BindRead();
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	const int width = game.window->GetWidth();
	const int height = game.window->GetHeight();
	glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	multiSampleFbo.UnbindRead();
	glDisable(GL_FRAMEBUFFER_SRGB);

	//RenderFrameBuffer(&renderFbo, finalMat);
}

void Renderer3D::RenderFrameBuffer(const GLFrameBuffer * fbo, const Material * material)
{
	glEnable(GL_CULL_FACE);

	material->GetShader()->Enable();
	material->SetShaderProperties();
	frameMesh->BindBuffers(0);
	glActiveTexture(GL_TEXTURE0); // needs to be before fbo bind
	fbo->GetTexture()->Bind();
	{
		material->GetShader()->SetUniformI("frameBuffer", 0);
		const GLuint indexCount = frameMesh->indexBuffers[0]->GetCount();
		glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, nullptr);
	}
	fbo->GetTexture()->Unbind();
	frameMesh->UnbindBuffers(0);
	material->GetShader()->Disable();

	glDisable(GL_CULL_FACE);
}

bool Renderer3D::SortRenderQueue(const Renderable & a, const Renderable & b, const GLCamera * camera)
{
	if (a.material->GetTransparency() == Material::translucent &&
		b.material->GetTransparency() == Material::translucent)
	{
		Vec3 cameraPos = Vec3(camera->GetViewMat().rawColumns[3]);
		float distA = (cameraPos - Vec3(a.transform.rawColumns[3])).Lenght();
		float distB = (cameraPos - Vec3(b.transform.rawColumns[3])).Lenght();
		return distA > distB;
	}
	else if (a.material->GetTransparency() == Material::translucent ||
			 b.material->GetTransparency() == Material::translucent)
	{
		return a.material->GetTransparency() != Material::translucent;
	}
	else
	{
		if (a.material->GetShader() == b.material->GetShader())
		{
			return a.material->GetId() < b.material->GetId();
		}
		return a.material->GetShader()->GetId() < b.material->GetShader()->GetId();
	}
}
