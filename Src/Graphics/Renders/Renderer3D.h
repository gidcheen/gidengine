#pragma once

#include <vector>

#include "Assets/Mesh.h"
#include "Graphics/LightStructs.h"
#include "Graphics/Buffers/GLFrameBuffer.h"


using namespace std;

class GLCamera;

struct Renderable
{
	Mesh * mesh;
	int materialIndex;
	Material * material;
	Mat4 transform;

	Renderable(Mesh * mesh, int materialIndex, Material * material, const Mat4 & transform) :
		mesh(mesh),
		materialIndex(materialIndex),
		material(material),
		transform(transform)
	{
	}
};

class Renderer3D
{
private:
	vector<const GLCamera *> mainCameras;
	vector<Renderable> renderQueue;
	GLFrameBuffer multiSampleFbo;
	GLFrameBuffer renderFbo;

	vector<AmbientLight> ambientLights;
	vector<DirectionalLight> directionalLights;
	vector<PointLight> pointLights;
	vector<SpotLight> spotLights;

	AssetRef<Mesh> frameMesh;
	AssetRef<Material> finalMat;

	const string alUniformName = "ambientLight.";
	const string dlUniformName = "directionalLight.";
	const string plUniformName = "pointLight.";
	const string slUniformName = "spotLight.";

	int numDrawCalls;

public:
	Renderer3D();

	void SubmitCamera(const GLCamera * camera) { mainCameras.push_back(camera); }
	void SubmitMesh(Mesh * mesh, const vector<Material *> & materials, const Mat4 & transform);
	void SubmitAmbientLight(const AmbientLight & al) { ambientLights.push_back(al); }
	void SubmitDirectionalLight(const DirectionalLight & dl) { directionalLights.push_back(dl); }
	void SubmitPointLight(const PointLight & pl) { pointLights.push_back(pl); }
	void SubmitSpotLight(const SpotLight & sl) { spotLights.push_back(sl); }

	void Render(const GLCamera * camera, const Material * overrideMaterial = nullptr, bool depthOnly = false);
	void Flush();

private:
	void RenderFrameBuffer(const GLFrameBuffer * fbo, const Material * material);
	static bool SortRenderQueue(const Renderable & a, const Renderable & b, const GLCamera * camera);
};
