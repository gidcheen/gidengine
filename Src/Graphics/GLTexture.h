#pragma once

#include "Utils/Assert.h"
#include "Graphics/GL.h"
#include "Game.h"


enum class Wrapping
{
	repeat, mirroredRepeat, clamp
};
enum class Filtering
{
	linear, nearest
};

class GLTexture
{
public:
	struct TextureSettings
	{
		static TextureSettings Create(int channelCount, bool isCubeMap, Wrapping wrapping, Filtering filtering)
		{
			TextureSettings ts = {
				target: GLenum(isCubeMap ? GL_TEXTURE_CUBE_MAP : GL_TEXTURE_2D),
				internalFormat: GLTexture::GetGLChannel(channelCount),
				format: GLTexture::GetGLChannel(channelCount),
				type: GL_UNSIGNED_BYTE,
				channelCount: channelCount,
				elementSize: sizeof(unsigned char),
				wrapping: wrapping,
				filtering: filtering,
				anisotropic: game.graphicsSettings.anisotropic,
				mipmapEnabled: true,
				sampleCount: 1
			};
			return ts;
		}

		GLenum target = GL_TEXTURE_2D;
		GLenum internalFormat = GL_RGB;
		GLenum format = GL_RGB;
		GLenum type = GL_UNSIGNED_BYTE;

		int channelCount = 3;
		int elementSize = sizeof(unsigned char);

		Wrapping wrapping = Wrapping::repeat;
		Filtering filtering = Filtering::linear;
		int anisotropic = 1;
		bool mipmapEnabled = true;

		int sampleCount = 1;
	};

private:
	unsigned int textureId;
	int width;
	int height;
	TextureSettings set;

public:
	GLTexture(unsigned char * data, int width, int height, const TextureSettings & settings);
	virtual ~GLTexture() { glDeleteTextures(1, &textureId); }

	inline unsigned int GetTextureId() const { return textureId; };
	inline int GetWidth() const { return width; }
	inline int GetHeight() const { return height; }


	void Set(unsigned char * data, int width, int height, const TextureSettings * settings = nullptr);

	inline void Bind() const { glBindTexture(set.target, textureId); }
	inline void Unbind() const { glBindTexture(set.target, 0); }

	static GLenum GetWrappingType(Wrapping wrapping);
	static GLenum GetGLChannel(int channelCount)
	{
		switch (channelCount)
		{
		case 1:
			return GL_RED;
		case 2:
			return GL_RG;
		case 3:
			return GL_RGB;
		case 4:
			return GL_RGBA;
		default:
			ASSERT(false);
		}
	}

private:
	void CreateTexture(unsigned char * data, int width, int height, GLenum target);
	static bool CanMipmap(GLenum target);
};
