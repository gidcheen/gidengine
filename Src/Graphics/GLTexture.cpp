#include "GLTexture.h"

#include "Utils/Assert.h"


GLTexture::GLTexture(unsigned char * data, int width, int height, const TextureSettings & settings) :
	textureId(0),
	width(0),
	height(0),
	set(settings)
{
	glGenTextures(1, &textureId);
	Set(data, width, height);
}

void GLTexture::Set(unsigned char * data, int width, int height, const TextureSettings * settings)
{
	this->width = width;
	this->height = height;
	if (settings) { this->set = *settings; }

	Bind();
	{

		if (set.sampleCount <= 1)
		{
			GLenum glWrapping = GetWrappingType(set.wrapping);
			glTexParameteri(set.target, GL_TEXTURE_WRAP_S, glWrapping);
			glTexParameteri(set.target, GL_TEXTURE_WRAP_T, glWrapping);
			glTexParameteri(set.target, GL_TEXTURE_WRAP_R, glWrapping);

			if (set.filtering == Filtering::linear)
			{
				glTexParameteri(set.target, GL_TEXTURE_MIN_FILTER, set.mipmapEnabled ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
				glTexParameteri(set.target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			}
			else
			{
				glTexParameteri(set.target, GL_TEXTURE_MIN_FILTER, set.mipmapEnabled ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST);
				glTexParameteri(set.target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			}

			GLfloat largest;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest);
			glTexParameterf(set.target, GL_TEXTURE_MAX_ANISOTROPY_EXT, set.anisotropic <= largest ? set.anisotropic : largest);
		}

		if (set.target == GL_TEXTURE_CUBE_MAP)
		{
			ASSERT(height % 6 == 0);
			int singleHeight = height / 6;
			for (int i = 0; i < 6; i++)
			{
				GLenum target = (GLenum)GL_TEXTURE_CUBE_MAP_POSITIVE_X + i;
				int offset = i * width * singleHeight * set.channelCount * set.elementSize;
				unsigned char * start = data + offset;
				CreateTexture(start, width, singleHeight, target);
			}
		}
		else
		{
			CreateTexture(data, width, height, set.target);
		}

		if (set.mipmapEnabled && set.sampleCount == 1 && CanMipmap(set.target)) { glGenerateMipmap(set.target); }
	}
	Unbind();
}

GLenum GLTexture::GetWrappingType(Wrapping wrapping)
{
	switch (wrapping)
	{
	case Wrapping::repeat:
		return GL_REPEAT;
	case Wrapping::mirroredRepeat:
		return GL_MIRRORED_REPEAT;
	case Wrapping::clamp:
		return GL_CLAMP_TO_EDGE;
	}
}

void GLTexture::CreateTexture(unsigned char * data, int width, int height, GLenum target)
{
	if (set.sampleCount > 1)
	{
		glTexImage2DMultisample(target, set.sampleCount, set.internalFormat, width, height, GL_TRUE);
	}
	else
	{
		glTexImage2D(target, 0, set.internalFormat, width, height, 0, set.format, set.type, data);
	}
}

bool GLTexture::CanMipmap(GLenum target)
{
	switch (target)
	{
	case GL_TEXTURE_1D:
	case GL_TEXTURE_2D:
	case GL_TEXTURE_3D:
	case GL_TEXTURE_1D_ARRAY:
	case GL_TEXTURE_2D_ARRAY:
	case GL_TEXTURE_CUBE_MAP:
	case GL_TEXTURE_CUBE_MAP_ARRAY:
		return true;
	default:
		return false;
	}
}
