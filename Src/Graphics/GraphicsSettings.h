#pragma once

struct GraphicsSettings
{
	const int msaa;
	const int anisotropic;
};