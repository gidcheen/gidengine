#pragma once

#include "Maths/Maths.h"

class GLTexture;

struct LightType
{
	enum
	{
		ambient = 0,
		directional,
		point,
		spot
	};
};

struct AmbientLight
{
	Vec4 color = Vec4(0.0, 0.0, 0.0, 0.0);
};

struct DirectionalLight
{
	Vec4 color = Vec4(0.0, 0.0, 0.0, 0.0);
	Vec3 direction = Vec3(0, 0, 0);
	const GLTexture * shadowDepth;
};

struct PointLight
{
	Vec4 color = Vec4(0.0, 0.0, 0.0, 0.0);
	Vec3 position = Vec3(0, 0, 0);
	float range = 0;
	const GLTexture * shadowDepth;
};

struct SpotLight
{
	Vec4 color = Vec4(0.0, 0.0, 0.0, 0.0);
	Vec3 position = Vec3(0, 0, 0);
	Vec3 direction = Vec3(0, 0, 0);
	float innerCutOff = 0;
	float outerCutOff = 0;
	float range = 0;
	const GLTexture * shadowDepth;
};
