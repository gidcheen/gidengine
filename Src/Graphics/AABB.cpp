#include "AABB.h"


AABB & AABB::Transform(const Mat4 & trans)
{
	c += trans.ToPosition();

	// abs rot matrix multiplication
	float rx = abs(trans[0][0]) * r.x + abs(trans[1][0]) * r.y + abs(trans[2][0]) * r.z;
	float ry = abs(trans[0][1]) * r.x + abs(trans[1][1]) * r.y + abs(trans[2][1]) * r.z;
	float rz = abs(trans[0][2]) * r.x + abs(trans[1][2]) * r.y + abs(trans[2][2]) * r.z;

	r = Vec3(rx, ry, rz);
}
