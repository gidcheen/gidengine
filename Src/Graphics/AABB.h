#pragma once

#include <array>

#include "Maths/Maths.h"


using namespace std;


class AABB
{
public:
	Vec3 c;
	Vec3 r;

public:
	AABB() = default;
	AABB(const Vec3 & c, const Vec3 & r) : c(c), r(r) {}

	bool Overlaps(const AABB & other)
	{

		bool x = abs(c.x - other.c.x) <= (r.x + other.r.x);
		bool y = abs(c.y - other.c.y) <= (r.y + other.r.y);
		bool z = abs(c.z - other.c.z) <= (r.z + other.r.z);
		return x && y && z;
	}

	AABB & Transform(const Mat4 & trans);
	AABB Transformed(const Mat4 & trans) { return AABB(*this).Transform(trans); }
};
