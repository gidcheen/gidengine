#pragma once

#include <vector>
#include <string>

#include "GL.h"
#include "PropertySystem/Property.h"


using namespace std;

class GLTexture;

class GLShader
{
public:
	struct ShaderSrcs
	{
		string vertSrc;
		string tessSrc;
		string geomSrc;
		string fragSrc;
	};

private:
	GLuint shaderId;

public:
	GLShader(const ShaderSrcs & srcs);
	~GLShader();

	inline void Enable() const { glUseProgram(shaderId); }
	inline void Disable() const { glUseProgram(0); }


	void SetUniformF(const string & name, float value) const;
	void SetUniformI(const string & name, int value) const;
	void SetUniformV2(const string & name, const Vec2 & value) const;
	void SetUniformV3(const string & name, const Vec3 & value) const;
	void SetUniformV4(const string & name, const Vec4 & value) const;
	void SetUniformMat4(const string & name, const Mat4 & value) const;
	void SetTexture(const string & name, const GLTexture * texture, int position) const;

private:
	GLint GetUniformLocation(const string & name) const;
	void CompileShader(GLuint shader, const string & src);
};
