#include "Window.h"

#include "Utils/Assert.h"
#include "Utils/Logging.h"

#include "GameHeader.h"


using namespace std;

Window::Window(const char * name, int width, int height, bool fullscreen) :
	name(name),
	width(width),
	height(height),
	fullscreen(fullscreen),
	closed(false),
	isMouseHidden(false),
	isMouseCaptured(false)
{
	keyStatuses.fill(false);
	lastKeyStatuses.fill(false);
	mouseButonsStatuses.fill(false);
	lastMouseButonsStatuses.fill(false);

	if (!Init())
	{
		LOG_ERROR("Window could not be initializend");
	}
}

void Window::Update()
{
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		LOG_ERROR("OpenGL Error" + to_string(error));
	}

	lastKeyStatuses = keyStatuses;
	lastMouseButonsStatuses = mouseButonsStatuses;
	lastMousePos = mousePos;

	if (isMouseCaptured)
	{
		Vec2 screenCenter(width / 2.f, height / 2.f);
		glfwSetCursorPos(glfwWindow, screenCenter.x, screenCenter.y);
		mousePos = screenCenter;
	}

	resChanged = false;

	glfwPollEvents();
	glfwGetFramebufferSize(glfwWindow, &width, &height);
	glfwSwapBuffers(glfwWindow);
}

bool Window::ShouldClose() const
{
	return glfwWindowShouldClose(glfwWindow) != 0;
}

bool Window::IsKeyPressed(unsigned int keycode)
{
	ASSERT(keycode < MAX_KEYS);
	return keyStatuses[keycode];
}

bool Window::IsKeyDown(unsigned int keycode)
{
	ASSERT(keycode < MAX_KEYS);
	return keyStatuses[keycode] && !lastKeyStatuses[keycode];
}

bool Window::IsKeyUp(unsigned int keycode)
{
	ASSERT(keycode < MAX_KEYS);
	return !keyStatuses[keycode] && lastKeyStatuses[keycode];
}

bool Window::IsMouseButtonPressed(unsigned int mButton)
{
	ASSERT(mButton < MAX_MOUSE_BUTTONS);
	return mouseButonsStatuses[mButton];
}

Vec2 Window::GetRelMousePos()
{
	if (isMouseCaptured)
	{
		return mousePos - Vec2(width / 2.f, height / 2.f);
	}
	else
	{
		return mousePos - lastMousePos;
	}
}

void Window::SetIsMouseHidden(bool hidden)
{
	isMouseHidden = hidden;
	if (hidden)
	{
		glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}
	else
	{
		glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void Window::SetIsMouseCaptured(bool captured)
{
	isMouseCaptured = captured;
	if (captured)
	{
		glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		// move mouse to screen center
		Vec2 screenCenter(width / 2.f, height / 2.f);
		glfwSetCursorPos(glfwWindow, screenCenter.x, screenCenter.y);
		mousePos = screenCenter;
		lastMousePos = screenCenter;
	}
	else
	{
		glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

bool Window::Init()
{
	if (!glfwInit())
	{
		LOG_ERROR("Error initializing glfw");
	}

	// window settings
	// antialiasing // should be done by frame buffer // seems like it must match frambuffer on nvidia for srgb blit
	glfwWindowHint(GLFW_SAMPLES, game.graphicsSettings.msaa);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // OpenGL 4.5
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE); // set opengl debug to true
	glfwSwapInterval(0); // disable vsync // doesnt seem to work for mesa amd on linux

	if (fullscreen)
	{
		glfwWindow = glfwCreateWindow(width, height, name, glfwGetPrimaryMonitor(), nullptr);
	}
	else
	{
		glfwWindow = glfwCreateWindow(width, height, name, nullptr, nullptr);
	}

	if (!glfwWindow)
	{
		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(glfwWindow);
	glfwSetWindowUserPointer(glfwWindow, this);
	glfwSetWindowSizeCallback(glfwWindow, WindowResize);
	glfwSetKeyCallback(glfwWindow, KeyCallback);
	glfwSetMouseButtonCallback(glfwWindow, MouseButtonCallback);
	glfwSetCursorPosCallback(glfwWindow, CursorPositionCallback);

	// glew setup
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		LOG_ERROR("could not init glew");
		return false;
	}

	const GLubyte * renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte * version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// if debug was successful
	GLint flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (!(flags & GL_CONTEXT_FLAG_DEBUG_BIT)) { ASSERT(false); }
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(GLDebugCallback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	// debug setup end

	// first clear
	Clear();
	glfwSwapBuffers(glfwWindow);

	return true;
}

void Window::KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	Window * w = (Window *)glfwGetWindowUserPointer(window);
	w->keyStatuses[key] = action != GLFW_RELEASE;
}

void Window::MouseButtonCallback(GLFWwindow * window, int button, int action, int mods)
{
	Window * w = (Window *)glfwGetWindowUserPointer(window);
	w->mouseButonsStatuses[button] = action != GLFW_RELEASE;
}

void Window::CursorPositionCallback(GLFWwindow * window, double x, double y)
{
	Window * w = (Window *)glfwGetWindowUserPointer(window);
	w->mousePos = Vec2((float)x, (float)y);
}

void Window::WindowResize(GLFWwindow * window, int width, int height)
{
	glViewport(0, 0, width, height);

	Window * w = (Window *)glfwGetWindowUserPointer(window);
	w->width = width;
	w->height = height;
	w->resChanged = true;
}
void Window::GLDebugCallback(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar * message,
	const void * userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204 || id==131076) { return; }

	string log;
	log += "Debug message(" + to_string(id) + "): " + string(message) + "\n";

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		log += "Source: API";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		log += "Source: Window System";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		log += "Source: Shader Compiler";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		log += "Source: Third Party";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		log += "Source: Application";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		log += "Source: Other";
		break;
	default:
		ASSERT(false);
		break;
	}
	log += '\n';

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		log += "Type: Error";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		log += "Type: Deprecated Behaviour";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		log += "Type: Undefined Behaviour";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		log += "Type: Portability";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		log += "Type: Performance";
		break;
	case GL_DEBUG_TYPE_MARKER:
		log += "Type: Marker";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		log += "Type: Push Group";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		log += "Type: Pop Group";
		break;
	case GL_DEBUG_TYPE_OTHER:
		log += "Type: Other";
		break;
	default:
		ASSERT(false);
		break;
	}
	log += '\n';

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		log += "Severity: high";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		log += "Severity: medium";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		log += "Severity: low";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		log += "Severity: notification";
		break;
	default:
		ASSERT(false);
		break;
	}

	if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
	{
		//LOG(log);
	}
	else
	{
		LOG_ERROR(log);
		ASSERT(false);
	}
}
