#pragma once

#include "Maths/Maths.h"
#include "Assets/Material.h"


class GLFrameBuffer;

class GLCamera
{
public:
	vector<AssetRef<Material>> postEffects;

private:
	Mat4 viewMat;
	Mat4 projectionMat;
	GLFrameBuffer * frameBuffer; // todo relace with rendertexture asset

public:
	GLCamera(const Mat4 & projectionMat, const Mat4 & viewMat, GLFrameBuffer * framebuffer = nullptr) :
		viewMat(viewMat),
		projectionMat(projectionMat),
		frameBuffer(framebuffer) {}

	inline void SetViewMat(const Mat4 & mat) { viewMat = mat; }
	inline const Mat4 & GetViewMat() const { return viewMat; }
	inline void SetProjectionMat(const Mat4 & mat) { projectionMat = mat; }
	inline const Mat4 & GetProjectionMat() const { return projectionMat; }
	inline void SetFrameBuffer(GLFrameBuffer * fbo) { frameBuffer = fbo; }
	inline const GLFrameBuffer * GetFrameBuffer() const { return frameBuffer; }
};
