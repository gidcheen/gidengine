#pragma once

#include "Maths/Maths.h"


class Vertex
{
public:
	Vertex() = default;
	Vertex(Vec3 position, Vec3 normal, Vec3 tangent, Vec3 bitangent, Vec3 color, Vec2 uv) :
		position(position),
		normal(normal),
		tangent(tangent),
		bitangent(bitangent),
		color(color),
		uv(uv)
	{
	}

	Vec3 position;
	Vec3 normal;
	Vec3 tangent;
	Vec3 bitangent;
	Vec3 color;
	Vec2 uv;
};
