#include "GLShader.h"

#include "GLTexture.h"

GLShader::GLShader(const ShaderSrcs & srcs) :
	shaderId(glCreateProgram())
{
	// mandatory programmes
	ASSERT(srcs.fragSrc.size() > 0);
	ASSERT(srcs.vertSrc.size() > 0);
	bool useTess = srcs.tessSrc.size() > 0;
	bool useGeom = srcs.geomSrc.size() > 0;

	GLuint vert = glCreateShader(GL_VERTEX_SHADER);
	CompileShader(vert, srcs.vertSrc);

	GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
	CompileShader(frag, srcs.fragSrc);

	GLuint tess = 0;
	if (useTess)
	{
		tess = glCreateShader(GL_TESS_CONTROL_SHADER);
		CompileShader(tess, srcs.tessSrc);
	}

	GLuint geom = 0;
	if (useGeom)
	{
		geom = glCreateShader(GL_GEOMETRY_SHADER);
		CompileShader(geom, srcs.geomSrc);
	}

	glLinkProgram(shaderId);

	glValidateProgram(shaderId);

	glDeleteShader(vert);
	glDeleteShader(frag);
	if (useTess) { glDeleteShader(tess); }
	if (useGeom) { glDeleteShader(geom); }
}

GLShader::~GLShader()
{
	glDeleteProgram(shaderId);
}

void GLShader::SetUniformF(const string & name, float value) const
{
	int loc = GetUniformLocation(name);
	glUniform1f(loc, value);
}

void GLShader::SetUniformI(const string & name, int value) const
{
	int loc = GetUniformLocation(name);
	glUniform1i(loc, value);
}

void GLShader::SetUniformV2(const string & name, const Vec2 & value) const
{
	int loc = GetUniformLocation(name);
	glUniform2f(loc, value.x, value.y);
}

void GLShader::SetUniformV3(const string & name, const Vec3 & value) const
{
	int loc = GetUniformLocation(name);
	glUniform3f(loc, value.x, value.y, value.z);
}

void GLShader::SetUniformV4(const string & name, const Vec4 & value) const
{
	int loc = GetUniformLocation(name);
	glUniform4f(loc, value.x, value.y, value.z, value.w);
}

void GLShader::SetUniformMat4(const string & name, const Mat4 & value) const
{
	int loc = GetUniformLocation(name);
	glUniformMatrix4fv(loc, 1, GL_FALSE, &value[0][0]);
}

void GLShader::SetTexture(const string & name, const GLTexture * texture, int position) const
{
	SetUniformI(name, position);
	glActiveTexture(GL_TEXTURE0 + (GLenum)position);
	texture->Bind();
}

GLint GLShader::GetUniformLocation(const string & name) const
{
	return glGetUniformLocation(shaderId, name.c_str());
}

void GLShader::CompileShader(GLuint shader, const string & src)
{
	const char * srcC = src.c_str();

	glShaderSource(shader, 1, &srcC, nullptr);
	glCompileShader(shader);

	GLint result;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		GLint lenght;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &lenght);
		char error[1024];
		glGetShaderInfoLog(shader, lenght, &lenght, &error[0]);
		LOG_ERROR("shader error" + string(error));
		glDeleteShader(shader);
		ASSERT(false);
	}
	glAttachShader(shaderId, shader);
}
