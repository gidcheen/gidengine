#pragma once

#include <GL/glew.h>

#include "Graphics/GLTexture.h"


class GLFrameBuffer
{
private:
	GLuint bufferId;
	GLTexture * texture;
	GLTexture * depthTexture;

public:
	GLFrameBuffer(int width, int height, int sampleCount = 1, bool useColor = true, bool useDepth = true);
	~GLFrameBuffer();

	GLuint GetBufferId() const { return bufferId; }
	const GLTexture * GetTexture() const { return texture; }
	const GLTexture * GetDepthTexture() const { return depthTexture; }

	void Resize(int width, int height);

	inline void Clear() const
	{
		Bind();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		Unbind();
	}

	void Bind() const { glBindFramebuffer(GL_FRAMEBUFFER, bufferId); }
	void Unbind() const { glBindFramebuffer(GL_FRAMEBUFFER, 0); }

	void BindRead() const { glBindFramebuffer(GL_READ_FRAMEBUFFER, bufferId); }
	void UnbindRead() const { glBindFramebuffer(GL_READ_FRAMEBUFFER, 0); }

	void BindWrite() const { glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bufferId); }
	void UnbindWrite() const { glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); }
};
