#pragma once

#include <GL/glew.h>


class GLIndexBuffer
{
private:
	GLuint bufferId;
	GLuint count;

public:
	GLIndexBuffer(GLuint * data, GLuint count);
	~GLIndexBuffer();

	void Bind() const;
	void Unbind() const;

	GLuint GetCount() const;
};
