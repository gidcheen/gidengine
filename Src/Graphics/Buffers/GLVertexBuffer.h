#pragma once

#include <GL/glew.h>


class GLVertexBuffer
{
private:
	GLuint bufferId;
	GLuint size;
	GLuint vertexCount;
	bool isStatic;

public:
	GLVertexBuffer(GLfloat * data, GLuint count, GLuint vertexCount, bool isStatic = true);
	~GLVertexBuffer();

	void Bind() const;
	void Unbind() const;

	void UpdateData(const GLfloat * data, GLuint size, GLint offset = 0);

	GLuint GetSize() const;
	GLuint GetVertexCount() const;
};
