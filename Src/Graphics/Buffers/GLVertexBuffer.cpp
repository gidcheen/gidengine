#include "GLVertexBuffer.h"

#include "Utils/Logging.h"


GLVertexBuffer::GLVertexBuffer(GLfloat * vertices, GLuint count, GLuint vertexCount, bool isStatic) :
	size(sizeof(GLfloat) * count),
	vertexCount(vertexCount),
	isStatic(isStatic)
{
	glGenBuffers(1, &bufferId);
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	if (isStatic)
	{
		glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
	}
	else
	{
		glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_DYNAMIC_DRAW);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLVertexBuffer::~GLVertexBuffer()
{
	glDeleteBuffers(1, &bufferId);
}

void GLVertexBuffer::Bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
}

void GLVertexBuffer::Unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GLVertexBuffer::UpdateData(const GLfloat * data, GLuint count, GLint offset)
{
	if (isStatic)
	{
		LOG("Trying to update static buffer");
		return;
	}
	if (count * sizeof(GLfloat) + offset > size)
	{
		LOG("GLVertexBuffer overrun while updating, but probably ok");
	}
	glBufferSubData(GL_ARRAY_BUFFER, 0, count * sizeof(GLfloat), data);
}

GLuint GLVertexBuffer::GetSize() const
{
	return size;
}

GLuint GLVertexBuffer::GetVertexCount() const
{
	return vertexCount;
}
