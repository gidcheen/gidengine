#pragma once

#include <vector>

#include "Graphics/GL.h"

#include "GLVertexBuffer.h"
#include "GLIndexbuffer.h"


using namespace std;

class GLVertexArray
{
private:
	GLuint vertexArrayId;
	vector<GLVertexBuffer *> vertexBuffers;

public:
	GLVertexArray();
	~GLVertexArray();

	void AddVertexBuffer(GLVertexBuffer * vertexBuffer, GLuint index, GLint componentCount, int stride, int offset);

	void Bind() const;
	void Unbind() const;
};
