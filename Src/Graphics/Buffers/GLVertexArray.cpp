#include "GLVertexArray.h"

#include <algorithm>

#include "Utils/Logging.h"


GLVertexArray::GLVertexArray()
{
	glGenVertexArrays(1, &vertexArrayId);
}

GLVertexArray::~GLVertexArray()
{
	for (size_t i = 0; i < vertexBuffers.size(); i++)
	{
		delete vertexBuffers[i];
	}
}

void GLVertexArray::AddVertexBuffer(
	GLVertexBuffer * vertexBuffer,
	GLuint index,
	GLint componentCount,
	int stride,
	int offset)
{
	if (std::find(vertexBuffers.begin(), vertexBuffers.end(), vertexBuffer) == vertexBuffers.end())
	{
		vertexBuffers.push_back(vertexBuffer);
	}

	Bind();
	vertexBuffer->Bind();

	glEnableVertexAttribArray(index);
	glVertexAttribPointer(
		index,
		componentCount,
		GL_FLOAT,
		GL_FALSE,
		stride * sizeof(float),
		(void *)(offset * sizeof(float)));

	//glVertexAttribPointer(index, componentCount, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);

	vertexBuffer->Unbind();
	Unbind();
}

void GLVertexArray::Bind() const
{
	glBindVertexArray(vertexArrayId);
}

void GLVertexArray::Unbind() const
{
	glBindVertexArray(0);
}
