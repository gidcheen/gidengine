#include "GLFrameBuffer.h"

#include "Utils/Assert.h"
#include "GameHeader.h"


GLFrameBuffer::GLFrameBuffer(int width, int height, int sampleCount, bool useColor, bool useDepth) :
	bufferId(0),
	texture(nullptr),
	depthTexture(nullptr)
{
	glGenFramebuffers(1, &bufferId);
	Bind();
	{
		GLenum target = sampleCount > 1 ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
		if (useColor)
		{
			// color buffer
			GLTexture::TextureSettings tsColor;
			tsColor.target = target;
			tsColor.sampleCount = sampleCount;
			tsColor.mipmapEnabled = false;
			texture = new GLTexture(nullptr, width, height, tsColor);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target, texture->GetTextureId(), 0);
		}
		else
		{
			// disable color buffer
			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);
		}

		if (useDepth)
		{
			// depth & stencil buffer
			GLTexture::TextureSettings tsDepth;
			tsDepth.target = target;
			tsDepth.internalFormat = GL_DEPTH_COMPONENT;// GL_DEPTH24_STENCIL8;
			tsDepth.format = GL_DEPTH_COMPONENT;// GL_DEPTH_STENCIL;
			tsDepth.type = GL_FLOAT; //GL_UNSIGNED_INT_24_8;
			tsDepth.sampleCount = sampleCount;
			tsDepth.mipmapEnabled = false;
			depthTexture = new GLTexture(nullptr, width, height, tsDepth);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, target, depthTexture->GetTextureId(), 0);
		}

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			LOG_ERROR("GLFrameBuffer creation failed");
		}
	}
	Unbind();
	Clear();
}

GLFrameBuffer::~GLFrameBuffer()
{
	delete texture;
	delete depthTexture;
	glDeleteFramebuffers(1, &bufferId);
}

void GLFrameBuffer::Resize(int width, int height)
{
	texture->Set(nullptr, width, height);
	depthTexture->Set(nullptr, width, height);
}
