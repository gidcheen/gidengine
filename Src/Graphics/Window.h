#pragma once

#include <iostream>
#include <array>
#include "Graphics/GL.h"
#include <GLFW/glfw3.h>

#include "Utils/Logging.h"
#include "Maths/Mat.h"


#define MAX_KEYS (GLFW_KEY_LAST + 1)
#define MAX_MOUSE_BUTTONS 32

class Window
{
private:
	GLFWwindow * glfwWindow;

	const char * name;
	int width;
	int height;
	bool closed;
	bool fullscreen;
	bool resChanged;

	array<bool, MAX_KEYS> keyStatuses;
	array<bool, MAX_KEYS> lastKeyStatuses;
	array<bool, MAX_MOUSE_BUTTONS> mouseButonsStatuses;
	array<bool, MAX_MOUSE_BUTTONS> lastMouseButonsStatuses;
	Vec2 mousePos;
	Vec2 lastMousePos;
	bool isMouseHidden;
	bool isMouseCaptured;

public:
	Window(const char * name, int width, int height, bool fullscreen = false);
	~Window() { glfwTerminate(); };

	inline int GetWidth() const { return width; };
	inline int GetHeight() const { return height; };
	inline bool GetFullscreen() const { return fullscreen; }
	inline bool GetResChanged() const { return resChanged; }

	// OpenGl stuff
	inline void Clear() const { glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); };
	void Update();
	bool ShouldClose() const;

	// input
	// keyboard
	bool IsKeyPressed(unsigned int keycode);
	bool IsKeyDown(unsigned int keycode);
	bool IsKeyUp(unsigned int keycode);

	// mouse
	bool IsMouseButtonPressed(unsigned int mButton);
	inline Vec2 GetMousePos() { return mousePos; };
	Vec2 GetRelMousePos();
	inline bool GetIsMouseHidden() { return isMouseHidden; };
	void SetIsMouseHidden(bool hidden);
	inline bool GetIsMouseCaptured() { return isMouseCaptured; };
	void SetIsMouseCaptured(bool captured);

	inline GLFWwindow * GetGLFWWindow() const { return glfwWindow; };

private:
	bool Init();
	static void WindowResize(GLFWwindow * window, int width, int height);
	static void KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods);
	static void MouseButtonCallback(GLFWwindow * window, int button, int action, int mods);
	static void CursorPositionCallback(GLFWwindow * window, double x, double y);
	static void GLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * userParam);
};
