#pragma once

#include <string>
#include "Graphics/GraphicsSettings.h"


using namespace std;

class Window;

class AssetImporter;

class AssetManager;

class Renderer3D;

class Scene;

namespace reactphysics3d
{
	class DynamicsWorld;
}
namespace rp = reactphysics3d;

class Game
{
public:
	Window * window;

	AssetImporter * assetImporter;
	AssetManager * assetManager;
	Renderer3D * renderer3D;
	Scene * scene;
	rp::DynamicsWorld * physics;

	const string assetsDir = "../Assets/"; // todo: make config file
	const string importsDir = "Imports/"; // todo: make config file
	const GraphicsSettings graphicsSettings = { msaa:2, anisotropic:16 }; // todo: make config file

	void Construct();
	void Destruct();

	int MainLoop();
};

extern Game game;
