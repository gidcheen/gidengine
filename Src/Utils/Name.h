#pragma once

#include "Hash.h"


class Name
{
private:
	ID id;
	string name;

public:
	Name() :
		id(ToHash(name)),
		name("") {}

	Name(const string & name) :
		id(ToHash(name)),
		name(name) {}

	Name(const char * name) :
		Name(string(name)) {}

	Name & operator=(const string & other)
	{
		*this = Name(other);
		return *this;
	}

	operator ID() const { return GetId(); }
	operator string() const { return ToString(); }

	bool operator==(const Name & other) const { return id == other.id; }

	inline ID GetId() const { return id; }
	inline const string & ToString() const { return name; }
};
