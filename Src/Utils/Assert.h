#pragma once

#include <cassert>

#include "Logging.h"


#if defined(DEBUG) || defined(ASSERT_DEBUG)
#	define ASSERT(x) if (!(x)) { LOG_ERROR("Assert error!"); assert(x); }
#else
#	define ASSERT(x) if (!(x)) { LOG_ERROR("Assert error!"); assert(x); } // todo: black screen without assert.
#endif // DEBUG
