#pragma once


#include "Utils/Name.h"


using namespace std;

class Named
{
public:
	virtual Name GetName() const = 0;
};

#define NAME(n) public: static Name Get##n##Name() { return Name(#n); } virtual Name GetName() const override { return Get##n##Name(); }
