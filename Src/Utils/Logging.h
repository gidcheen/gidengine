#pragma once

#include <string>
#include <iostream>

using namespace std;


#define LOG(MSG) Logging::Log(MSG, __FILE__, __LINE__)
#define LOG_ERROR(MSG) Logging::Log(MSG, __FILE__, __LINE__, true)


class Logging
{
public:
	static void Log(const string& msg, const string& file, int line, bool error = false);

private:
	static string RemovePath(const string & str);
};
