#include "Hash.h"

#include "Assert.h"

ID ToHash(const string & str)
{
	ID hash = 619ULL;
	const char * s = str.c_str();
	while (*s)
	{
		hash = hash * 101ULL + *s++;
	}
	ASSERT(hash);
	return hash;
}
