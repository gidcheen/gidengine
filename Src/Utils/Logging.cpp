#include "Logging.h"
#include "StringUtils.h"


void Logging::Log(const string & msg, const string & file, int line, bool error)
{
	string type(error ? "ERROR" : "LOG");
	string log("\n");
	log += type + " >>----  ------\n";
	log += "\n" + msg + "\n\n";
	log += "in File: " + RemovePath(file) + ", Line: " + to_string(line) + "\n";
	log += "------  ----<< " + type;
	if (error)
	{
		cerr << log << endl;
	}
	else
	{
		cout << log << endl;
	}
}

string Logging::RemovePath(const string & str)
{
	string s = str;
	s = StringUtils::FixPath(s);
	s = StringUtils::SplitString(s, '/').back();
	return s;
}
