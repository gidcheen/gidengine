#pragma once

#include <string>
#include <vector>
#include <functional>

#include "Name.h"


using namespace std;

template<typename TType, typename TData>
class DynamicFactory
{
private:
	struct Constructor
	{
		const Name name;
		const function<TType *(TData &)> constructor;
	};
private:
	vector<Constructor> constructors;

public:
	void Add(const Name & name, function<TType *(TData &)> constructor)
	{
		constructors.push_back({ name, constructor });
	}

	TType * Create(const Name & name, TData & data)
	{
		for (const Constructor & c : constructors)
		{
			if (name == c.name)
			{
				return c.constructor(data);
			}
		}
		return nullptr;
	}
};
