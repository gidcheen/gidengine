#pragma once

#include <vector>
#include <string>
#include <algorithm>
#include <experimental/filesystem>

#include "PropertySystem/PropertyBase.h"


using namespace std;
namespace fs = std::experimental::filesystem;

class StringUtils
{
public:
	static string ToLower(const string & s)
	{
		string s2 = s;
		//transform(s2.begin(), s2.end(), s2.begin(), tolower);
		return s2;
	}

	static vector<string> SplitString(const string & s, char c)
	{
		vector<string> ret;
		string currentS;
		for (char cc : s)
		{
			if (cc == c)
			{
				if (currentS.size() == 0) { continue; }
				ret.push_back(currentS);
				currentS = "";
				continue;
			}
			currentS += cc;
		}
		if (currentS.size() > 0) { ret.push_back(currentS); };
		return ret;
	}

	static vector<string> SplitString(const string & src, const string & delimiter)
	{
		vector<string> ret;
		string s = src;
		size_t pos = 0;
		std::string token;
		while ((pos = s.find(delimiter)) != string::npos)
		{
			if (pos != 0) { ret.push_back(s.substr(0, pos)); }
			s.erase(0, pos + delimiter.length());
		}
		ret.push_back(s);
		return ret;
	}

	static string FixPath(string & ps)
	{
		replace(ps.begin(), ps.end(), '\\', '/');
		if (ps[0] == (char)34)
		{
			ps.erase(ps.begin());
			ps.pop_back();
		}
		return ps;
	}

	static string GetFileEnding(const string & path)
	{
		unsigned long start = path.find_last_of('.');
		if (start == string::npos) { return ""; }
		string ending(path.begin() + start + 1, path.end());
		ending = StringUtils::ToLower(ending);
		return ending;
	}

	static fs::path ToPath(const string & s)
	{
		fs::path path;
		path += s;
		return path;
	}

	static PropertyBase * CreateProperty(const string & s)
	{
		vector<string> splitS = SplitString(s, ' ');
		ASSERT(splitS.size() >= 3);
		string typeS = splitS[0];
		string nameS = splitS[1];
		string valueS;
		for (int i = 2; i < splitS.size(); i++) { valueS += splitS[i]; }

		Json::Value jsonV;
		Json::Reader reader;
		ASSERT(reader.parse(valueS, jsonV));

		const TypeBase * type = TypeBase::GetType(typeS);
		PropertyBase * p = new PropertyBase(type, type->fromJson(jsonV), nameS);
		return p;
	}
};
