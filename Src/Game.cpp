#include "Game.h"
#include "GameHeader.h"

#include "Assets/AssetManagement/Importers/ImporterMaterial.h"
#include "Assets/AssetManagement/Importers/ImporterMesh.h"
#include "Assets/AssetManagement/Importers/ImporterShader.h"
#include "Assets/AssetManagement/Importers/ImporterTexture.h"

#include "Assets/AssetManagement/Loaders/LoaderMaterial.h"
#include "Assets/AssetManagement/Loaders/LoaderMesh.h"
#include "Assets/AssetManagement/Loaders/LoaderShader.h"
#include "Assets/AssetManagement/Loaders/LoaderTexture.h"

#include "Scene/Components/MeshComponent.h"
#include "Scene/Components/CameraComponent.h"
#include "Scene/Components/LightComponent.h"
#include "Scene/Components/FpsComponent.h"
#include "Scene/Components/MeshSpawner.h"
#include "Scene/Components/RigidBodyComponent.h"


void Game::Construct()
{
	ios::sync_with_stdio(false);

	// ---- creation of the game window ----
	window = new Window("Gid Engine", 1400, 800);
	//window = new Window("Gid Engine", 1920, 1080, true);

	// ---- engine initialisation ----

	// set up importer
	assetImporter = new AssetImporter();
	assetImporter->AddImporter(new ImporterMaterial);
	assetImporter->AddImporter(new ImporterMesh());
	assetImporter->AddImporter(new ImporterShader());
	assetImporter->AddImporter(new ImporterTexture());
#ifdef EDITOR
	assetImporter->ImportAllAssets();
#endif //EDITOR

	// set up asset manager
	assetManager = new AssetManager();
	assetManager->AddAssetPipeline(AssetPipeline(Material::GetMaterialName(), new LoaderMaterial));
	assetManager->AddAssetPipeline(AssetPipeline(Mesh::GetMeshName(), new LoaderMesh()));
	assetManager->AddAssetPipeline(AssetPipeline(Shader::GetShaderName(), new LoaderShader()));
	assetManager->AddAssetPipeline(AssetPipeline(Texture::GetTextureName(), new LoaderTexture()));

	renderer3D = new Renderer3D();
	scene = new Scene();

	auto & cf = scene->GetComponentFactory();
	cf.Add(CameraComponent::GetCameraComponentName(), [](CompData & c) { return new CameraComponent(c.e, c.j); });
	cf.Add(FpsComponent::GetFpsComponentName(), [](CompData & c) { return new FpsComponent(c.e, c.j); });
	cf.Add(LightComponent::GetLightComponentName(), [](CompData & c) { return new LightComponent(c.e, c.j); });
	cf.Add(MeshComponent::GetMeshComponentName(), [](CompData & c) { return new MeshComponent(c.e, c.j); });
	cf.Add(MeshSpawner::GetMeshSpawnerName(), [](CompData & c) { return new MeshSpawner(c.e, c.j); });
	cf.Add(
		RigidBodyComponent::GetRigidBodyComponentName(), [](CompData & c) { return new RigidBodyComponent(c.e, c.j); });

	physics = new rp::DynamicsWorld(rp3d::Vector3(0.0, -8.0f, 0.0));
	physics->setNbIterationsVelocitySolver(20);
	physics->setNbIterationsPositionSolver(10);
}

void Game::Destruct()
{
	// ---- game teardown ----

	delete physics;
	delete scene;
	delete renderer3D;
	delete assetManager;
	delete assetImporter;
	delete window;
}

int Game::MainLoop()
{
	Construct();


	Entity * root = new Entity(nullptr, "Root");

	// Scene testing
#ifndef asdf
	auto createHallway = [=](int i)->void
	{
		Entity * hallway = new Entity(root, "hallway");
		MeshComponent * hwmc = new MeshComponent(hallway);
		hallway->transform.SetPosition(Vec3(0, 0, -i * 2 - 12));
		hwmc->SetMesh("Models/Hallway.dae");
		hwmc->SetMaterials({ "Materials/Glass.material", "Materials/Metal.material", "Materials/Scifi.material" });

		Entity * light = new Entity(hallway, "light");
		light->transform.SetPosition(Vec3(0, 1.85, 0));
		LightComponent * lc = new LightComponent(light);
		lc->lightType = LightType::point;
		lc->color = Vec4(1, 1, 1, 1);
		lc->strenght = 0.5f;
	};

	for (int i = 0; i < 40; i++)
	{
		createHallway(i);
	}


	Entity * sciFiRoom = new Entity(root, "sciFiRoom ");
	MeshComponent * sfrmc = new MeshComponent(sciFiRoom);
	sfrmc->SetMesh("Models/SciFiRoom.dae");
	sfrmc->SetMaterials(
		{
			"Materials/Metal.material",
			"Materials/SciFi01.material",
			"Materials/SciFi02.material",
			"Materials/SciFi03.material",
			"Materials/SciFi04.material",
			"Materials/SciFi05.material" });

	Entity * centerBox = new Entity(sciFiRoom, "centerBox");
	auto cbmc = new MeshComponent(centerBox);
	cbmc->SetMesh("Models/CenterBox.dae");
	cbmc->SetMaterials({ "Materials/Glass.material", "Materials/Metal.material" });

	Entity * centerTree = new Entity(sciFiRoom, "centerTree");
	auto ctmc = new MeshComponent(centerTree);
	ctmc->SetMesh("Models/Tree.dae");
	ctmc->SetMaterials({ "Materials/Bark.material" });

	Entity * ambientLight = new Entity(root, "light");
	LightComponent * ambientLC = new LightComponent(ambientLight);
	ambientLC->lightType = LightType::ambient;
	ambientLC->color = Vec4(1, 1, 1, 1);
	ambientLC->strenght = 0.1;

	Entity * directionalLight = new Entity(root, "light");
	directionalLight->transform.SetPosition(Vec3(0, 5.5, 0));
	directionalLight->transform.SetRotation(Vec3(120, 0, 0));
	LightComponent * directionalLC = new LightComponent(directionalLight);
	directionalLC->lightType = LightType::directional;
	directionalLC->strenght = 0.0;

	LightComponent * bigPL = new LightComponent(directionalLight);
	bigPL->lightType = LightType::point;
	bigPL->strenght = 30;


	auto tor = [&](int i)
	{
		Entity * torus2 = new Entity(root, "torus2");
		torus2->transform.SetPosition(Vec3(-0, 8 + i, -5));
		torus2->transform.SetScale(Vec3(0.5, 0.5, 0.5));
		MeshComponent * meshComponent5 = new MeshComponent(torus2);
		meshComponent5->SetMesh("Models/Sphere02.dae");
		meshComponent5->SetMaterials({ "Materials/Metal.material" });
		auto rb = new RigidBodyComponent(torus2);
		rb->SetMass(2);
		rb->SetAngularDamp(0.3);
		rb->SetAngularVelocity(Vec3(2, 5, 0));
		vector<PropertyBase *> ps01;
		ps01.push_back(new PropertyBase(&IDType, new ID(ToHash("Sphere")), "type"));
		ps01.push_back(new PropertyBase(&FloatType, new float(0.5), "radius"));
		rb->AddCollision(new Collision(ps01));
	};

	for (int i = 0; i < 5; i++)
	{
		tor(i);
	}


	Entity * floor = new Entity(root);
	floor->transform.SetPosition(Vec3(0, -1, 0));
	auto rb02 = new RigidBodyComponent(floor);
	rb02->SetRigidBodyType(RigidBodyComponent::STATIC);
	vector<PropertyBase *> ps02;
	ps02.push_back(new PropertyBase(&IDType, new ID(ToHash("Box")), "type"));
	ps02.push_back(new PropertyBase(&FloatType, new float(60), "x"));
	ps02.push_back(new PropertyBase(&FloatType, new float(01), "y"));
	ps02.push_back(new PropertyBase(&FloatType, new float(60), "z"));
	rb02->AddCollision(new Collision(ps02));


	// camera
	Entity * camera = new Entity(root, "camera");
	camera->transform.SetPosition(Vec3(0, 3, 8));
	new CameraComponent(camera);
	new FpsComponent(camera);
	new MeshSpawner(camera);
	Entity * spotLight = new Entity(camera, "light");
	spotLight->transform.SetRotation(Vec3(0, 180, 0));
	LightComponent * spotLC = new LightComponent(spotLight);
	spotLC->lightType = LightType::spot;
	spotLC->strenght = 0.7;


	Entity * skybox = new Entity(root, "skybox");
	skybox->transform.SetScale({ 30, 30, 30 });
	MeshComponent * sbmc = new MeshComponent(skybox);
	sbmc->SetMesh("Models/Skybox.dae");
	sbmc->SetMaterials({ "Materials/Skybox.material" });

	Entity * skyMan = new Entity(root, "skyMan");
	skyMan->transform.SetPosition({ 2, 0, 2 });
	auto * skyManMeshC = new MeshComponent(skyMan);
	skyManMeshC->SetMesh("Models/BaseMale.dae");
	skyManMeshC->SetMaterials({ "Materials/Skybox.material" });



	//scene->SaveScene(hallway, "../../Scene.json");
	//scene->SaveScene("../../Scene.json");
#endif

	//Entity * newRoot = new Entity(game.scene->GetRootEntity(), "new root");
	//newRoot->transform.SetPosition(Vec3(2, -2, 5));
	//scene->LoadScene(newRoot, "../../Scene.json");
	//scene->LoadScene("../../Scene.json");

	//rb->SetAngularVelocity(Vec3(1, 0, 0));
	LOG("YO\n\nYO");
	window->SetIsMouseCaptured(true);

	double dt = 0;
	double lastDts[3] = { 1.0 / 60.0, 1.0 / 60.0, 1.0 / 60.0 };
	double currentTime = 0;
	double lastTime = 0;
	while (!window->ShouldClose())
	{
		// delta time calculation
		currentTime = glfwGetTime();
		lastDts[2] = lastDts[1];
		lastDts[1] = lastDts[0];
		lastDts[0] = currentTime - lastTime;
		lastTime = currentTime;
		dt = (lastDts[0] + lastDts[1] + lastDts[2]) / 3;
		//LOG(to_string(1 / dt));
		if (dt > 1 / 15.0) { dt = 1 / 15.0; }


		window->Clear();

		scene->Awake();
		scene->Destroy();
		scene->Start();
		scene->Stop();
		scene->Update(dt);
		scene->PrePhysics();
		physics->update((float)dt);
		scene->PostPhysics();
		scene->Render();

		renderer3D->Flush();

		window->Update();
	}

	Destruct();
	return 0;
}
