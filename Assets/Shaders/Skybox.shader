<----> defaults <---->
TextureRef skyboxTex 0

<----> vert <---->

#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 tangent;
layout (location = 3) in vec3 bitangent;
layout (location = 4) in vec3 color;
layout (location = 5) in vec2 uv;

uniform mat4 projectionMat = mat4(1);
uniform mat4 viewMat = mat4(1);
uniform mat4 modelMat = mat4(1);

out VSOut
{
	vec3 uv;
} vsOut;

void main()
{
	vsOut.uv = inverse(mat3(viewMat)) * vec3(viewMat * modelMat * vec4(position, 1));
	gl_Position = projectionMat * viewMat * modelMat * vec4(position, 1);
} 

<----> frag <---->

#version 450

struct Material
{
	samplerCube skybox;
};
uniform Material material;

in VSOut
{
	vec3 uv;
} fsIn;

out vec4 outColor;

void main()
{
	outColor = texture(material.skybox, fsIn.uv);
}
