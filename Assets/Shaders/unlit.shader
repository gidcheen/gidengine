<----> defaults <---->
Vec4 color {"x": 1,"y": 1,"z": 1,"w": 1}

<----> vert <---->

#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 tangent;
layout (location = 3) in vec3 bitangent;
layout (location = 4) in vec3 color;
layout (location = 5) in vec2 uv;

uniform mat4 projectionMat = mat4(1.0);
uniform mat4 viewMat = mat4(1.0);
uniform mat4 modelMat = mat4(1.0);

void main()
{
	gl_Position = projectionMat * viewMat * modelMat * vec4(position, 1);
} 

<----> frag <---->

#version 450

struct Material
{
	vec4 color;
};
uniform Material material;

out vec4 outColor;

void main()
{
	outColor = material.color;
}
