<----> defaults <---->

<----> vert <---->

#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 tangent;
layout (location = 3) in vec3 bitangent;
layout (location = 4) in vec3 color;
layout (location = 5) in vec2 uv;

out vec2 ouv;

void main()
{
    ouv = uv;
	gl_Position = vec4(position.x, position.y, 0.0, 1.0); ;
} 

<----> frag <---->

#version 450

in vec2 ouv;

uniform sampler2D frameBuffer;

out vec4 outColor;

void main()
{
	outColor = texture(frameBuffer, ouv);
    //outColor = vec4(1, 0, 0, 1);
}
