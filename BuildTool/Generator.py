from abc import abstractmethod, ABCMeta
from ConfigData import *

class Generator(object):
	"""Base class for all project file generators"""

	__metaclass__ = ABCMeta

	def __init__(self):
		pass

	@abstractmethod
	def Generate(self, configdata):
		print("Generating: ")
		print(configdata.includeLibs)
