import xml.etree.cElementTree as ET
from xml.dom.minidom import parse, parseString
from Generator import *
from ConfigData import *
from Utils import *
import shutil

class VSGenerator(Generator):
	"""The VS specific generator"""

	def __init__(self):
		super().__init__()

	def Generate(self, configData):
		super().Generate(configData)
		self.CreatePrjFile(configData)
		self.CreateUserFile()
		self.CreateSolFile()

		#delete .vs folder. not removing it causes vs to freeze. this deletes all breakpoints and open files
		try:
			shutil.rmtree("../.vs")
		except:
			pass


	def CreatePrjFile(self, configData):
		#root
		Project = ET.Element("Project", {"DefaultTargets": "Build",
							 "ToolsVersion": "15.0",
							 "xmlns": "http://schemas.microsoft.com/developer/msbuild/2003"})


		# Project configuration deffinition
		ProjectConfigurations = ET.SubElement(Project, "ItemGroup", {"Label": "ProjectConfigurations"})
		ProjectConfiguration1 = ET.SubElement(ProjectConfigurations, "ProjectConfiguration", {"Include": "Game-Debug|x64"})
		ET.SubElement(ProjectConfiguration1, "Configuration").text = "Game-Debug"
		ET.SubElement(ProjectConfiguration1, "Platform").text = "x64"

		ProjectConfiguration2 = ET.SubElement(ProjectConfigurations, "ProjectConfiguration", {"Include": "Game-Release|x64"}) 
		ET.SubElement(ProjectConfiguration2, "Configuration").text = "Game-Release"
		ET.SubElement(ProjectConfiguration2, "Platform").text = "x64"

		ProjectConfiguration3 = ET.SubElement(ProjectConfigurations, "ProjectConfiguration", {"Include": "Editor-Debug|x64"}) 
		ET.SubElement(ProjectConfiguration3, "Configuration").text = "Editor-Debug"
		ET.SubElement(ProjectConfiguration3, "Platform").text = "x64"

		ProjectConfiguration4 = ET.SubElement(ProjectConfigurations, "ProjectConfiguration", {"Include": "Editor-Release|x64"}) 
		ET.SubElement(ProjectConfiguration4, "Configuration").text = "Editor-Release"
		ET.SubElement(ProjectConfiguration4, "Platform").text = "x64"


		# globals (?)
		Global = ET.SubElement(Project, "PropertyGroup", {"Label": "Globals"})
		ET.SubElement(Global, "VCProjectVersion").text = "15.0"
		ET.SubElement(Global, "RootNamespace").text = "GidEngine"
		ET.SubElement(Global, "ProjectName").text = "GidEngine"
		ET.SubElement(Global, "WindowsTargetPlatformVersion").text = "10.0.15063.0"
		
		# ---- imports ----
		ET.SubElement(Project, "Import", {"Project": "$(VCTargetsPath)\Microsoft.Cpp.Default.props"})


		# ---- general settings ----

		generalSettings = ET.SubElement(Project, "PropertyGroup", {"Label": "Configuration"})
		#generalSettings = ET.SubElement(Project, "PropertyGroup", {"Condition":
		#"'$(Configuration)|$(Platform)'=='Editor-Debug|x64'", "Label":
		#"Configuration"})
		ET.SubElement(generalSettings, "ConfigurationType").text = "Application"
		ET.SubElement(generalSettings, "UseDebugLibraries").text = "true"
		ET.SubElement(generalSettings, "PlatformToolset").text = "v141"
		ET.SubElement(generalSettings, "CharacterSet").text = "MultiByte"

		# ---- general settings ----

		# ---- imports ----
		ET.SubElement(Project, "Import", {"Project": "$(VCTargetsPath)\Microsoft.Cpp.props"})


		# ---- code files ----

		# header files
		headerFiles = ET.SubElement(Project, "ItemGroup")
		for hF in GetAllHeaderFiles():
			ET.SubElement(headerFiles, "ClInclude", {"Include": hF})

		# source files
		sourceFiles = ET.SubElement(Project, "ItemGroup")
		for srcF in GetAllSourceFiles():
			ET.SubElement(sourceFiles, "ClCompile", {"Include": srcF})

		# ---- code files ----


		# ---- VC++ Directories ----

		#
		importGroup = ET.SubElement(Project, "ImportGroup", {"Label": "PropertySheets"})
		ET.SubElement(importGroup, "Import", {"Project": "$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props",
											  "Condition": "exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')",
											  "Label": "LocalAppDataPlatform"})

		vcppDirs = ET.SubElement(Project, "PropertyGroup")
		ET.SubElement(vcppDirs, "OutDir").text = "$(SolutionDir)Bin/"
		ET.SubElement(vcppDirs, "IntDir").text = "$(SolutionDir)Bin/Intermidiate/$(Configuration)/"

		# include dirs
		includeText = "$(IncludePath);$(SolutionDir)/Src;"
		for id in configData.includeDirs:
			includeText += "$(SolutionDir)Dep/" + id + ";"
		ET.SubElement(vcppDirs, "IncludePath").text = includeText

		# lib dirs
		libText = "$(LibraryPath);"
		for ld in configData.libDirs:
			libText += "$(SolutionDir)Dep/" + ld + ";"
		ET.SubElement(vcppDirs, "LibraryPath").text = libText

		# ---- VC++ Directories ----

		# ---- C++ settings ----

		cppSettings = ET.SubElement(Project, "ItemDefinitionGroup")
		ClCompile = ET.SubElement(cppSettings, "ClCompile")
		ET.SubElement(ClCompile, "WarningLevel").text = "Level3"
		ET.SubElement(ClCompile, "Optimization").text = "Disabled"
		#ET.SubElement(ClCompile, "SDLCheck").text = "true"
		link = ET.SubElement(cppSettings, "Link")
		ET.SubElement(link, "SubSystem").text = "Console"

		# ---- C++ settings ----

		# ---- Linker Input ----

		linkerInput = ET.SubElement(Project, "ItemDefinitionGroup")
		linkerInput.append(ET.Element("Link"))
		ilText = ""
		for il in configData.includeLibs:
			ilText += il + ";"
		ET.SubElement(linkerInput[0], "AdditionalDependencies").text = ilText		

		defineDefault = ET.SubElement(linkerInput, "ClCompile")
		ET.SubElement(defineDefault, "PreprocessorDefinitions").text = "GLEW_STATIC;%(PreprocessorDefinitions)"

		defineEditor = ET.SubElement(linkerInput, "ClCompile")
		ET.SubElement(defineEditor, "PreprocessorDefinitions", {"Condition": "'$(Configuration)'=='Editor-Debug'or'$(Configuration)'=='Editor-Release'"}).text = "GLEW_STATIC;EDITOR"

		defineGame = ET.SubElement(linkerInput, "ClCompile")
		ET.SubElement(defineGame, "PreprocessorDefinitions", {"Condition": "'$(Configuration)'=='Game-Debug'or'$(Configuration)'=='Game-Release'"}).text = "GLEW_STATIC;GAME"

		# ---- Linker Input ----


		# ---- imports ----
		ET.SubElement(Project, "Import", {"Project": "$(VCTargetsPath)\Microsoft.Cpp.targets"})


		# writing to file
		tree = ET.ElementTree(Project)
		pretty = parseString(ET.tostring(tree.getroot(),"utf-8")).toprettyxml(indent = "	")

		file = open("../GidEngine.vcxproj", "w")
		file.write(pretty)

	def CreateUserFile(self):
		Project = ET.Element("Project", {"ToolsVersion": "15.0",
										 "xmlns": "http://schemas.microsoft.com/developer/msbuild/2003"})
		propertyGroup = ET.SubElement(Project, "PropertyGroup")
		ET.SubElement(propertyGroup, "ShowAllFiles").text = "true"

		propertyGroup = ET.SubElement(Project, "PropertyGroup")
		ET.SubElement(propertyGroup, "LocalDebuggerWorkingDirectory").text = "$(SolutionDir)/Bin"

		tree = ET.ElementTree(Project)
		pretty = parseString(ET.tostring(tree.getroot(),"utf-8")).toprettyxml(indent = "	")

		file = open("../GidEngine.vcxproj.user", "w")
		file.write(pretty)

	def CreateSolFile(slef):
		file = "\n"
		file += "Microsoft Visual Studio Solution File, Format Version 12.00" + "\n"
		file += "# Visual Studio 15" + "\n"
		file += "VisualStudioVersion = 15.0.26430.13" + "\n"
		file += "MinimumVisualStudioVersion = 10.0.15063.0" + "\n"
		file +="Project(\"{}\") = \"GidEngine\", \"GidEngine.vcxproj\", \"{}\"" + "\n"
		file += "EndProject" + "\n"

		f = open("../GidEngine.sln", "w")
		f.write(file)

