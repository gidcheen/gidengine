import os

def GetAllSourceFiles():
	return GetAllFilesWithEnding(".cpp")

def GetAllHeaderFiles():
	return GetAllFilesWithEnding(".h")

def GetAllFilesWithEnding(ending):
	f = []
	for root, dirs, files in os.walk("../Src", topdown = True):
		for file in files:
			if not file.endswith(ending):
				continue
			f.append(os.path.join(root, file)[3:])
	return f