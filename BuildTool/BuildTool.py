import platform
from VSGenerator import *
from ConfigData import *

def main():
	if platform.system().startswith("Win"):
		includeDirs = ["assimp/include",
			"glew/include",
			"glfw/include",
			"jsoncpp/include",
			"reactphysics3d/include"]

		libDirs = ["assimp/win/Debug",
			"glew/win/Debug",
			"glfw/win/Debug",
			"jsoncpp/win/Debug",
			"reactphysics3d/win/Debug"]

		includeLibs = ["assimp-vc141-mt.lib",
			"zlibstatic.lib",
			"glfw3.lib",
			"glew32s.lib",
			"jsoncpp.lib",
			"reactphysics3d.lib",
			# build in
			"OpenGL32.lib",
			"gdi32.lib",
			"user32.lib",
			"Shell32.lib"]

	configData = ConfigData(includeDirs,
		libDirs,
		includeLibs)

	p = platform.system()
	print("Platform: ")
	if platform.system().startswith("Win"):
		print("Windows")
		gen = VSGenerator()
	else:
		print("Unknown")
		pass

	gen.Generate(configData)

if __name__ == "__main__":
	main()
	exit()
