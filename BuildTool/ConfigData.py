class ConfigData(object):
	"""Holds all the data to build the project"""
	
	def __init__(self, includeDirs, libDirs, includeLibs):
		self.includeDirs = includeDirs
		self.libDirs = libDirs
		self.includeLibs = includeLibs
